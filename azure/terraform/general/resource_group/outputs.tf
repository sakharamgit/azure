output "resource_group" {
    value = {
        name     = module.azurerm_resource_group.name
        location = module.azurerm_resource_group.location
    } 
}