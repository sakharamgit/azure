locals {
  config = jsondecode(file("terraform.tfvars.json"))
}

module "azurerm_resource_group" {
  source = "../../../../terraform/modules/azure/terraform-azurerm-resource-group"

  for_each = { for rg in local.config.resource_groups : rg.name => rg }

  resource_group_name     = each.value.name
  resource_group_location = each.value.location
  resource_group_prefix   = lookup(each.value, "prefix", "")
  resource_group_suffix   = lookup(each.value, "suffix", "")
  resource_tags           = each.value.resource_tags
  deployment_tags         = each.value.deployment_tags
}
