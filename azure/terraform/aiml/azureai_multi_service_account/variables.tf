##################################################################################
# VARIABLES
##################################################################################

# Common Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "ai"
}

variable "env" {
  type    = string
  default = "dev"
}

# Azure Variables
variable "az_location" {}
