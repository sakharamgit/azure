variable "resource_group_name" {
  description = "The name of the resource group"
  type        = string
  default     = "default_resource_group"
}

variable "location" {
  description = "The location of the resource group"
  type        = string
  default     = "East US"
}

variable "storage_account_name" {
  description = "The name of the storage account"
  type        = string
  default     = "defaultstorageaccountzxc"
}

variable "account_tier" {
  description = "The tier of the storage account"
  type        = string
  default     = "Standard"
}

variable "account_replication_type" {
  description = "The replication type of the storage account"
  type        = string
  default     = "LRS"
}

variable "tags" {
  description = "Tags to apply to the storage account"
  type        = map(string)
  default     = {}
}

variable "delete_retention_policy_in_days" {
  description = "The number of days that the blob should be retained for"
  type        = number
  default     = 90
}

variable "identity_type" {
  description = "The type of identity to assign to the storage account"
  type        = string
  default     = "SystemAssigned"
}

variable "resource_groups" {
  description = "The list of resource groups to create"
  type = map(object({
    resource_group_name     = string
    location = string
  }))
  default = {
    default_resource_group = {
      resource_group_name     = "default_resource_group"
      location = "East US"
      tags     = {}
    }
  }
}

variable "storage_accounts" {
  description = "The list of storage accounts to create"
  type = map(object({
    storage_account_name            = string
    resource_group_name             = string
    location                        = string
    account_tier                    = string
    account_replication_type        = string
    delete_retention_policy_in_days = number
    identity_type                   = string
    tags                            = map(string)
  }))
}

variable "common_tags" {
  description = "The common tags to apply to all resources"
  type        = map(string)
  default = {
    "tag" : "default"
  }
}

variable "global" {
  description = "Define global variables"
  type = map(string)
  default = {
    "variable" = "default"
  }
}