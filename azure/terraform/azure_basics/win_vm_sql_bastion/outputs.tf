#############################################################################################################
# OUTPUT
#############################################################################################################

output "azure_instance_public_dns" {
  value = azurerm_windows_virtual_machine.bastion.public_ip_address
}

output "private_ips" {
  value = azurerm_network_interface.example.*.ip_configuration
}