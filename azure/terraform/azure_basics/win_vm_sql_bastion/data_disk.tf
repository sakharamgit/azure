# To create a azure managed disk
resource "azurerm_managed_disk" "example" {
  count                = var.vm_instances_count
  name                 = "${var.prefix}-${var.project}-${var.env}-datadisk1-${count.index}"
  location             = azurerm_resource_group.example.location
  resource_group_name  = azurerm_resource_group.example.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 10
}

resource "azurerm_virtual_machine_data_disk_attachment" "example" {
  count              = var.vm_instances_count
  managed_disk_id    = azurerm_managed_disk.example[count.index].id
  virtual_machine_id = azurerm_windows_virtual_machine.example[count.index].id
  lun                = "10"
  caching            = "ReadWrite"
}