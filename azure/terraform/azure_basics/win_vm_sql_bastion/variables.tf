##################################################################################
# VARIABLES
##################################################################################

//Common Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "winvm"
}

variable "env" {
  type    = string
  default = "dev"
}

//Azure Variables
variable "azure_key_path" {}
variable "sample_txt_file_path" {}
variable "az_location" {}

// VM Image Variables
variable "publisher" {}
variable "offer" {}
variable "sku" {}
variable "osversion" {}


variable "az_vm_admin_username" {}
variable "az_vm_admin_password" {}

// VNet Variables
variable "az_vnet_address" {}
variable "az_subnet1_address" {}
variable "vm_instances_count" {}


variable "trusted_domain" {
  type = string
}

variable "foldername" {
  type = string
}