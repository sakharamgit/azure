Integration Azure Monitor with Service Now using Webhook and REST API:
Ref Article: 
https://docs.microsoft.com/en-us/azure/service-health/service-health-alert-webhook-servicenow

1. Navigate to 'System Security' -> Group and Create a new group

<secret> 5992526257
<group>	AzureAlertsGroup
<email>	admin@example.com

2. Full Integration URL
https://dev109039.service-now.com/api/489662/azureservicehealth?apiKey=5992526257