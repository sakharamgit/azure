#################################################
# Terraform script to create a an Azure Alert
#################################################

// Locals
locals {
  az_info          = yamldecode(file("./values.yaml"))
  prefix           = local.az_info.prefix
  env              = local.az_info.env
  project          = local.az_info.project
  snow_service_uri = local.az_info.snow_service_uri
  az_location      = local.az_info.az_location
  testemailaddress = local.az_info.testemailaddress
  common_tags = merge(
    { "environment" = local.env },
    { "terraform" = true }
  )
}

// To generate a random string
resource "random_string" "random" {
  length  = 5
  upper   = false
  lower   = true
  number  = true
  special = false
}

//To create a resource group
resource "azurerm_resource_group" "az-stoacc-snow-rg" {
  name     = "${local.prefix}-${local.project}-rg"
  location = local.az_location
}

// To create an monitor action group
resource "azurerm_monitor_action_group" "snow-webhook-grp" {
  name                = "${local.prefix}-${local.project}-actiongrp"
  resource_group_name = azurerm_resource_group.az-stoacc-snow-rg.name
  short_name          = "snowwebhook"

  email_receiver {
    name          = "testemailaddress"
    email_address = local.testemailaddress
  }

  webhook_receiver {
    name                    = "snow-webhook"
    service_uri             = local.snow_service_uri
    use_common_alert_schema = true
  }
}

// To create an Azure Alert
resource "azurerm_monitor_metric_alert" "example" {
  name                = "${local.prefix}-${local.project}-stoacc-transgt1"
  resource_group_name = azurerm_resource_group.az-stoacc-snow-rg.name
  scopes              = [azurerm_storage_account.az_storage_account.id]
  description         = "Action will be triggered when Transactions count is greater than 1."
  severity            = 2

  criteria {
    metric_namespace = "Microsoft.Storage/storageAccounts"
    metric_name      = "Transactions"
    aggregation      = "Total"
    operator         = "GreaterThan"
    threshold        = 0

    dimension {
      name     = "ApiName"
      operator = "Include"
      values   = ["*"]
    }
  }

  action {
    action_group_id = azurerm_monitor_action_group.snow-webhook-grp.id
  }
}