##################################################################################
# VARIABLES
##################################################################################

//Azure Variables
variable "RM_ARM_SUBSCRIPTION_ID" {}
variable "RM_ARM_CLIENT_ID" {}
variable "RM_ARM_CLIENT_SECRET" {}
variable "RM_ARM_TENANT_ID" {}