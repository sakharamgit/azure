# Deploy SQL Server databases (SaaS) on Azure
> This script will deploy:
 1. 2 sql servers in different zones
 2. 4 sql elastic pools (2 on each servers) 
 3. 2 databases on primary server, Added to elastic pool
 4. 2 secondaries databases on dr server, Added to elastic pool on dr server
 5. 1 Failover group with above 2 servers as partners and above 2 databases to the failover group
 5. 1 database on the primary and another geo-replicated database on secondary