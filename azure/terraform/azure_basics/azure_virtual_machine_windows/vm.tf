################################
# Data
################################

# To get the image id for creating a windows machine
# data "azurerm_platform_image" "windows" {
#   location  = var.az_location
#   publisher = var.publisher
#   offer     = var.offer
#   sku       = var.sku
#   version   = var.osversion
# }

// To source the template from a file
data "template_file" "windows" {
  template = file("${path.module}/files/winrm_config.ps1")
}

#####################################
# Resources
####################################

# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

# To create a virtual network
resource "azurerm_virtual_network" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-vnet"
  address_space       = var.az_vnet_address
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

# To create a subnet
resource "azurerm_subnet" "example" {
  name                 = "${var.prefix}-${var.project}-${var.env}-subnet1"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = var.az_subnet1_address
}

# To create a network interface
resource "azurerm_network_interface" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-nic"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.example.id
  }
}

# To create an public ip for the instance
resource "azurerm_public_ip" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-pip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Dynamic"
}

# To create a virtual machine
resource "azurerm_windows_virtual_machine" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-vm"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  size                = "Standard_F4"
  admin_username      = var.az_vm_admin_username
  admin_password      = var.az_vm_admin_password

  network_interface_ids = [
    azurerm_network_interface.example.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = var.publisher
    offer     = var.offer
    sku       = var.sku
    version   = var.osversion
  }
}


# Virtual Machine Extension to execute Powershell script
resource "azurerm_virtual_machine_extension" "example" {
  depends_on           = [azurerm_windows_virtual_machine.example]
  name                 = "${var.prefix}-${var.project}-${var.env}-vm-extension"
  virtual_machine_id   = azurerm_windows_virtual_machine.example.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"
  protected_settings   = <<SETTINGS
  {
    "commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(data.template_file.windows.rendered)}')) | Out-File -filepath install.ps1\" && powershell -ExecutionPolicy Unrestricted -File install.ps1"
  } 
  SETTINGS
  tags = {
    environment = var.env
  }
}

# To execute scripts on the virtual machine
resource "null_resource" "example" {
  depends_on = [azurerm_virtual_machine_extension.example]

  connection {
    type     = "winrm"
    host     = azurerm_windows_virtual_machine.example.public_ip_address
    user     = var.az_vm_admin_username
    password = var.az_vm_admin_password
  }
  provisioner "file" {
    source      = var.sample_ps_script1_path
    destination = "C:\\bootstrap\\sample_ps_script1.ps1"
  }
  provisioner "file" {
    source      = var.sample_ps_script2_path
    destination = "C:\\bootstrap\\sample_ps_script2.ps1"
  }

  provisioner "local-exec" {
    command     = "Set-Item WSMan:\\localhost\\client\\Trustedhosts -Value '${var.trusted_domain}' -Force"
    interpreter = ["PowerShell", "-Command"]
  }

  provisioner "remote-exec" {
    inline = [
      "Powershell C:\\bootstrap\\sample_ps_script1.ps1 -foldername ${var.foldername}",
      "Powershell C:\\bootstrap\\sample_ps_script2.ps1"
    ]
  }

}