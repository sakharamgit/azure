//Azure Credentials
azure_key_path       = "E:\\GitRepo\\automation\\common\\azure_terraform_user_key.pem"
sample_txt_file_path = "sample_txt_file.txt"

az_location = "eastus"

az_vm_admin_username = "winadmin"
az_vm_admin_password = "Password1!"

az_vnet_address    = ["10.1.0.0/16"]
az_subnet1_address = ["10.1.0.0/24"]


publisher = "MicrosoftSQLServer"
offer     = "SQL2016SP1-WS2016"
sku       = "Enterprise"
osversion = "13.2.210312"


sample_ps_script1_path = "./files/sample_ps_script1.ps1"
sample_ps_script2_path = "./files/sample_ps_script2.ps1"
trusted_domain         = "*.eastus.cloudapp.azure.com"
foldername             = "pstestfolder"