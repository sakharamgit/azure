##################################################################################
# VARIABLES
##################################################################################


//Azure Variables
# variable "subscription_id" {}
# variable "client_id" {}
# variable "client_secret" {}
# variable "tenant_id" {}

variable az_resource_group {}
variable az_location {}
variable az_vm_vnet {}
variable az_vnet_subnet {}
variable az_vm_name {}

variable "az_vm_admin_username" {}
variable "az_vm_admin_password" {}