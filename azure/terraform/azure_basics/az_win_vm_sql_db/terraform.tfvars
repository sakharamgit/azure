//Azure Credentials
// subscription_id = $env:SUBSCRIPTION_ID
// client_id       = $env:CLIENT_ID
// client_secret   = $env:CLIENT_SECRET
// tenant_id       = $env:TENANT_ID

az_resource_group = "az_win_sql_ss"
az_location       = "East US"
az_vm_vnet        = "az_vm_vnet"
az_vnet_subnet    = "az_vm_vnet_subnet"
az_vm_name        = "azwinvmmssql"

az_vm_admin_username = "winadmin"
az_vm_admin_password = "P@ssword1!"