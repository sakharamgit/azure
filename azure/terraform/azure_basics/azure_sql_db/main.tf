# To get the existing resource group
data "azurerm_resource_group" "example" {
  name = var.az_resource_group
}

# To get the existing sql server object
data "azurerm_mssql_server" "example" {
  name                = var.sql_server_name
  resource_group_name = var.az_resource_group
}

# To get the existing elastic pool object
data "azurerm_mssql_elasticpool" "example" {
  name                = var.sql_elastic_pool1
  resource_group_name = var.az_resource_group
  server_name         = var.sql_server_name
}

# To create a database on the sql server instance
resource "azurerm_mssql_database" "database4" {
  name         = var.database_name1
  server_id    = data.azurerm_mssql_server.example.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  zone_redundant  = true
  sample_name     = "AdventureWorksLT"
  elastic_pool_id = data.azurerm_mssql_elasticpool.example.id

  tags = {
    environment = "development"
  }

}

# To create a database on the sql server instance
resource "azurerm_mssql_database" "database5" {
  name         = var.database_name2
  server_id    = data.azurerm_mssql_server.example.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  zone_redundant  = true
  sample_name     = "AdventureWorksLT"
  elastic_pool_id = data.azurerm_mssql_elasticpool.example.id

  tags = {
    environment = "development"
  }

}

# To create a database on the sql server instance
resource "azurerm_mssql_database" "database6" {
  name         = var.database_name3
  server_id    = data.azurerm_mssql_server.example.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  zone_redundant  = true
  sample_name     = "AdventureWorksLT"
  elastic_pool_id = data.azurerm_mssql_elasticpool.example.id

  tags = {
    environment = "development"
  }

}