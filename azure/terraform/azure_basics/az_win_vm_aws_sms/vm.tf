################################
# Data
################################

// To source the template from a file
data "template_file" "windows" {
  template = file("${path.module}/files/winrm_config.ps1")
}

#####################################
# Resources
####################################

# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

# To create a network interface
resource "azurerm_network_interface" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-vm-nic"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.example.id
  }
}

# To create an public ip for the instance
resource "azurerm_public_ip" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-pip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Dynamic"
}

# To create a virtual machine
resource "azurerm_windows_virtual_machine" "example" {
  name                  = "${var.prefix}-${var.project}-${var.env}-vm"
  resource_group_name   = azurerm_resource_group.example.name
  location              = azurerm_resource_group.example.location
  size                  = "Standard_F4"
  admin_username        = var.winusername
  admin_password        = var.winpassword
  network_interface_ids = [azurerm_network_interface.example.id]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = var.publisher
    offer     = var.offer
    sku       = var.sku
    version   = var.osversion
  }
}

# Virtual Machine Extension to execute Powershell script
resource "azurerm_virtual_machine_extension" "example" {
  depends_on           = [azurerm_windows_virtual_machine.example]
  name                 = "${var.prefix}-${var.project}-${var.env}-vm-extension"
  virtual_machine_id   = azurerm_windows_virtual_machine.example.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"
  protected_settings   = <<SETTINGS
  {
    "commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(data.template_file.windows.rendered)}')) | Out-File -filepath install.ps1\" && powershell -ExecutionPolicy Unrestricted -File install.ps1"
  } 
  SETTINGS
  tags = {
    environment = var.env
  }
}

# To execute the AWS SMS  script
resource "null_resource" "execute_sms_script" {
  depends_on = [azurerm_virtual_machine_extension.example]

  connection {
    host     = azurerm_windows_virtual_machine.example.public_ip_address
    type     = "winrm"
    user     = var.winusername
    password = var.winpassword
  }

  provisioner "file" {
    source      = "./files/aws-sms-azure-setup.ps1"
    destination = "C:\\bootstrap\\aws-sms-azure-setup.ps1"
  }

  # provisioner "remote-exec" {
  #   inline = [
  #     "Powershell C:\\bootstrap\\aws-sms-azure-setup.ps1 -StorageAccountName ${azurerm_storage_account.example.name} -ExistingVNetName ${azurerm_virtual_network.example.name} -SubscriptionId ${var.subscription_id} -SubnetName ${azurerm_subnet.example.name}"
  #   ]
  #   on_failure = continue
  # }

}