##################################################################################
# VARIABLES
##################################################################################

//Common Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "sms"
}

variable "env" {
  type    = string
  default = "dev"
}

//Azure Variables
variable "azure_key_path" {}
variable "sample_txt_file_path" {}

variable subscription_id {
  type = string
}

resource "random_string" "random" {
  length  = 5
  upper   = false
  lower   = true
  number  = true
  special = false
}

variable "az_location" {}

variable "winusername" {}
variable "winpassword" {}

variable "vnet_cidr_range" {}
variable "subnet1_cidr_range" {}

// VM Image Variables
variable "publisher" {}
variable "offer" {}
variable "sku" {}
variable "osversion" {}
