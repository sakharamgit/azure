#################################################
# Terraform script to create a new resource group
#################################################

resource "azurerm_resource_group" "rg"{
    name = "test_resource_group"
    location = "eastus"
    tags = {
        createdby = "test_resource_group"
    }
}