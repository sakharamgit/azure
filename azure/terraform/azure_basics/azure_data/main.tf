# To get the image id for creating a virtual machine
data "azurerm_resource_group" "example" {
  name = "aeu2-a61-p-ws-rg01"
}

output "azurerm_rg" {
    value = data.azurerm_resource_group.example
}