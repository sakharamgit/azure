##################################################################################
# VARIABLES
##################################################################################


//Azure Variables
variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "azure_key_path" {}


variable "az_prefix" {
  type    = string
  default = "og"
}

variable "az_location" {
  type    = string
  default = "East US 2"
}

variable "environment" {
  type    = string
  default = "dev"
}
variable "functionapp" {
  type    = string
  default = "./build/functionapp.zip"
}

resource "random_string" "storage_name" {
  length  = 24
  upper   = false
  lower   = true
  number  = true
  special = false
}
