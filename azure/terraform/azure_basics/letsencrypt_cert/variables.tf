variable "server_url" {
  type        = string
  description = "The URL to the ACME endpoint's directory"
  default     = ""
}

variable "email_address" {
  type        = string
  description = "The contact email address for the account"
}

variable "min_days_remaining" {
  type        = number
  description = "The minimum amount of days remaining on the expiration of a certificate before a renewal is attempted"
  default     = 90
}

variable "common_name" {
  type        = string
  description = "The Private DNS Common Name"
  default     = ""
}

variable "subject_alternative_names" {
  type        = list(any)
  description = "The Private DNS SAN Name"
  default     = []
}

variable "keyvault_name" {
  type        = string
  description = "The kevaultname where certificate imported"
  default     = ""
}

variable "keyvault_resource_group" {
  type        = string
  description = "The resource group of keyvault"
  default     = ""
}

variable "dns_resource_group" {
  type        = string
  description = "The resource group of Private DNS"
  default     = ""
}
// Providers
# variable "AA_STG_ARM_SUBSCRIPTION_ID" {}
variable "AA_ARM_TENANT_ID" {}
variable "AA_ARM_CLIENT_ID" {}
variable "AA_ARM_CLIENT_SECRET" {}
variable "AA_LAB_ARM_SUBSCRIPTION_ID" {}
