terraform {
  required_version = ">= 0.13"
  required_providers {
    azurerm = ">= 2.28"
    acme = {
      source  = "vancluever/acme"
      version = "2.7.0"
    }
  }

  backend "azurerm" {
    resource_group_name  = "aeu2-2c2-cn-ws-rg01"
    storage_account_name = "terraformbackend"
    container_name       = "certs"
    key                  = "lab.terraform.tfstate"
    access_key           = "bRHcyhtvkaAXeI0HR65W41j1N4WSZXwYp7q9Bj1Tm5FxfMVFZciH90M9xPVXV/XaZMivJUHLHPsVskvpemq42g=="
    skip_provider_registration = true
  }
}


provider "azurerm" {
  subscription_id = var.AA_LAB_ARM_SUBSCRIPTION_ID
  client_id       = var.AA_ARM_CLIENT_ID
  client_secret   = var.AA_ARM_CLIENT_SECRET
  tenant_id       = var.AA_ARM_TENANT_ID
  features {}
}

provider "acme" {
  server_url = var.server_url
}

