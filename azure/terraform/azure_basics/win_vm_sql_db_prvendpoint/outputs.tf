#############################################################################################################
# OUTPUT
#############################################################################################################

output "azure_instance_public_dns" {
  value = azurerm_windows_virtual_machine.example.public_ip_address
}

output "az_sql_server_name" {
  value = azurerm_mssql_server.az_sql_server.fully_qualified_domain_name
}

output "sql_private_link_endpoint" {
  description = "SQL Private Link Endpoint IP"
  value = data.azurerm_private_endpoint_connection.og-endpoint-connection.private_service_connection.0.name
}
