#! /bin/bash

# To the user context to root user
sudo su

#To create a primary partition on the attach disk
echo "p w" | fdisk /dev/sdc

#To create a file system on the attached disk
echo "y" | mkfs -t ext4 /dev/sdc

#To create a hana installation directory
mkdir /hana

#To mount the disk to hana install directory
mount /dev/sdc /hana

#To change the current directory to tmp
cd /tmp

#To assign permission on the copied files
sudo chmod +x HXEDownloadManager_linux.bin
sudo chmod +x jre-8u231-linux-x64.rpm
sudo chmod 744 msawb-plugin-config-com-sap-hana.sh

# To install the Java Runtime
sudo rpm -ivh jre-8u231-linux-x64.rpm

# To downalod the sap hana db express edition
./HXEDownloadManager_linux.bin  --pp 8080 -d /tmp linuxx86_64 installer hxe.tgz
./HXEDownloadManager_linux.bin  --pp 8080 -d /tmp linuxx86_64 installer clients_linux_x86_64.tgz

#To extract the hana db and client setup files
tar -xvzf /tmp/hxe.tgz
tar -xvzf /tmp/clients_linux_x86_64.tgz
tar -xvzf /tmp/hdb_client_linux_x86_64.tgz

#To install the required packages
zypper -n install libatomic1*
zypper -n install insserv
zypper -n install libltdl7 

# # Manual Steps
# # To start the database server installation
# sudo /tmp/setup_hxe.sh

# # Reconnect to the virtual machine
# su - hxeadm

# # Navigate to client installation directory
# cd /tmp/HDB_CLIENT_LINUX_X86_64

# # Start client installation
# sudo ./hdbinst


