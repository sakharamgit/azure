#############################################################################################################
# RESOURCES
#############################################################################################################


//To create a resource group
resource "azurerm_resource_group" "example" {
  name     = var.az_rg_name
  location = var.az_rg_location
}


// To create a virtual network
resource "azurerm_virtual_network" "example" {
  name                = var.az_linux_vnet_name
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

// To create a virtual subnet
resource "azurerm_subnet" "example" {
  name                 = var.az_linux_subnet_name
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes = [
    "10.0.2.0/24"
  ]
}

// To create an public ip for the instance
resource "azurerm_public_ip" "example" {
  name                = "az_linux_vm_pip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Dynamic"
}

//To create a network interface to attach to the instance
resource "azurerm_network_interface" "example" {
  name                = var.az_vnet_nic_name
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "az-linux-nic-main"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.example.id
  }
}

// To create a network security group to allow traffic 
resource "azurerm_network_security_group" "example" {
  name                = "az_linux_mssql"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "ssh"
    priority                   = 100
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefixes    = ["0.0.0.0/0"]
    destination_port_range     = "22"
    destination_address_prefix = azurerm_network_interface.example.private_ip_address
  }
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "MSSQL"
    priority                   = 101
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefixes    = ["0.0.0.0/0"]
    destination_port_range     = "1433"
    destination_address_prefix = azurerm_network_interface.example.private_ip_address
  }
}

// To create a linux virtual machine
resource "azurerm_linux_virtual_machine" "example" {
  name                = var.az_linux_vm_name
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  size                = "Standard_D8as_v4"
  admin_username      = "adminuser"

  network_interface_ids = [
    azurerm_network_interface.example.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("..\\..\\..\\common\\azure_terraform_public_key.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  tags = {
    prometheus    = "true"
    node_exporter = "true"
    public_ip     = azurerm_public_ip.example.ip_address
  }

  source_image_reference {
    publisher = "SUSE"
    offer     = "sles-15-sp2"
    sku       = "gen1"
    version   = "latest"
  }
}

resource "azurerm_managed_disk" "example" {
  name                 = "sap-premium-disk"
  location             = azurerm_resource_group.example.location
  resource_group_name  = azurerm_resource_group.example.name
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = 1024
}

resource "azurerm_virtual_machine_data_disk_attachment" "example" {
  managed_disk_id    = azurerm_managed_disk.example.id
  virtual_machine_id = azurerm_linux_virtual_machine.example.id
  lun                = "0"
  caching            = "ReadWrite"
}

resource "null_resource" "file_copy" {
  depends_on = [azurerm_virtual_machine_data_disk_attachment.example]

  connection {
    type        = "ssh"
    host        = azurerm_linux_virtual_machine.example.public_ip_address
    user        = "adminuser"
    private_key = file(var.azure_key_path)
  }

  provisioner "file" {
    source      = "..\\..\\..\\files\\hanadb\\HXEDownloadManager_linux.bin"
    destination = "/tmp/HXEDownloadManager_linux.bin"
  }

  provisioner "file" {
    source      = "..\\..\\..\\files\\hanadb\\jre-8u231-linux-x64.rpm"
    destination = "/tmp/jre-8u231-linux-x64.rpm"
  }

  provisioner "file" {
    source      = "..\\..\\..\\files\\hanadb\\msawb-plugin-config-com-sap-hana.sh"
    destination = "/tmp/msawb-plugin-config-com-sap-hana.sh"
  }
}


# To execute the bash script
resource "azurerm_virtual_machine_extension" "example" {
  depends_on           = [azurerm_virtual_machine_data_disk_attachment.example]
  name                 = "hostname"
  virtual_machine_id   = azurerm_linux_virtual_machine.example.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"

  settings = <<SETTINGS
    {
        "script": "${base64encode(templatefile("./files/hana_configuration.sh",{}))}"
    }
SETTINGS
}



