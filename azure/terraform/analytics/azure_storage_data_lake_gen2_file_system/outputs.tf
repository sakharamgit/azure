#############################################################################################################
# OUTPUT
#############################################################################################################

output "azurerm_storage_account" {
  value = azurerm_storage_account.example.name
}

output "azurerm_resource_group" {
  value     = azurerm_resource_group.example.name
  sensitive = false
}