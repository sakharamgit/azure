//Azure Credentials
azure_key_path = "E:\\GitRepo\\automation\\common\\azure_terraform_user_key.pem"


az_location     = "Central India"
prefix          = "ss"
env             = "dev"
project         = "data"
az_stg_con_name = "db-dacpac"

blob_name_1      = "ActivityLog-01.csv"
blob_name_1_path = "./files/ActivityLog-01.csv"

blob_name_2      = "ActivityLog-01.json"
blob_name_2_path = "./files/ActivityLog-01.json"

blob_name_3      = "ActivityLog-01.parquet"
blob_name_3_path = "./files/ActivityLog-01.parquet"
