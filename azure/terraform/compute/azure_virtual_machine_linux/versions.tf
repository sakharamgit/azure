# Define the required versions
terraform {
  # Terraform Version
  required_version = ">= 1.2"
  # AzureRM Version
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.107.0"
    }
  }
} 