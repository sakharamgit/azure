#################################################
# Terraform script to create a new resource group
#################################################

//To create a resource group
resource "azurerm_resource_group" "azure-linux_vm_rg" {
  name     = "${var.az_prefix}-${var.az_environment}-${var.az_rg_name}"
  location = var.az_location
}

// To create a virtual network
resource "azurerm_virtual_network" "azure-linux_vm_vn" {
  name                = var.az_vm_name
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.azure-linux_vm_rg.location
  resource_group_name = azurerm_resource_group.azure-linux_vm_rg.name
}

// To create a virtual subnet
resource "azurerm_subnet" "azure-linux_vm_vs" {
  name                 = "azure-linux_vm_vs"
  resource_group_name  = azurerm_resource_group.azure-linux_vm_rg.name
  virtual_network_name = azurerm_virtual_network.azure-linux_vm_vn.name
  address_prefixes = [
    "10.0.2.0/24"
  ]
}

// To create an public ip for the instance
resource "azurerm_public_ip" "azure_linux_vm_pip" {
  name                = "azure-linux_vm_pip"
  resource_group_name = azurerm_resource_group.azure-linux_vm_rg.name
  location            = azurerm_resource_group.azure-linux_vm_rg.location
  allocation_method   = "Dynamic"
}

//To create a network interface to attach to the instance
resource "azurerm_network_interface" "azure_linux_vm_nic" {
  name                = "azure_linux_vm_nic"
  location            = azurerm_resource_group.azure-linux_vm_rg.location
  resource_group_name = azurerm_resource_group.azure-linux_vm_rg.name

  ip_configuration {
    name                          = "azure-minux_vm_nic"
    subnet_id                     = azurerm_subnet.azure-linux_vm_vs.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.azure_linux_vm_pip.id
  }
}

// To create a network security group to allow traffic 
resource "azurerm_network_security_group" "azure_linux_vm_nsg" {
  name                = "azure_linux_vm_nsg"
  location            = azurerm_resource_group.azure-linux_vm_rg.location
  resource_group_name = azurerm_resource_group.azure-linux_vm_rg.name
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "ssh"
    priority                   = 100
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefixes    = ["0.0.0.0/0"]
    destination_port_range     = "22"
    destination_address_prefix = azurerm_network_interface.azure_linux_vm_nic.private_ip_address
  }
}

// To create a linux virtual machine
resource "azurerm_linux_virtual_machine" "azure_linux_vm" {
  name                = "azure-linux-vm"
  resource_group_name = azurerm_resource_group.azure-linux_vm_rg.name
  location            = azurerm_resource_group.azure-linux_vm_rg.location
  size                = "Standard_F2"
  admin_username      = "adminuser"

  network_interface_ids = [
    azurerm_network_interface.azure_linux_vm_nic.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file(var.public_key_path)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  tags = {
    prometheus    = "true"
    node_exporter = "true"
    public_ip     = azurerm_public_ip.azure_linux_vm_pip.ip_address
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  connection {
    type        = "ssh"
    host        = azurerm_public_ip.azure_linux_vm_pip.ip_address
    user        = "adminuser"
    private_key = file(var.private_key_path)
  }

  #provisioner "file" {
  #  source      = var.sample_txt_file_path
  #  destination = "/tmp/sample_txt_file.txt"
  #}
}


resource "null_resource" "file_copy" {
  depends_on = [azurerm_linux_virtual_machine.azure_linux_vm]
  triggers = {
    file_copy = azurerm_linux_virtual_machine.azure_linux_vm.public_ip_address
  }

  connection {
    type        = "ssh"
    host        = azurerm_linux_virtual_machine.azure_linux_vm.public_ip_address
    user        = "adminuser"
    private_key = file(var.private_key_path)
  }

  provisioner "file" {
    source      = var.sample_txt_file_path
    destination = "/tmp/sample_txt_file.txt"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo add-apt-repository ppa:vbernat/haproxy-1.7 -y",
      "sudo apt update",
      "sudo apt install -y haproxy"
    ]
  }

}
