# Variable Values
private_key_path = "~/.ssh/terraformssh"
public_key_path  = "~/.ssh/terraformssh.pub"
sample_txt_file_path = "sample_txt_file.txt"

az_rg_name       = "vm-linux"
az_location      = "Central India"
az_environment   = "dev"
