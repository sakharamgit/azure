##################################################################################
# VARIABLES
##################################################################################

# Common Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "cb"
}

variable "env" {
  type    = string
  default = "dev"
}

# Azure Variables
variable "az_location" {}

variable "acr_pull_username" {
  type = string
}