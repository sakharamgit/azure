locals {
  container_names = ["bob", "kevin", "stewart"]
}

# To create an resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.az_prefix}-${var.az_environment}-${var.az_rg_name}"
  location = var.az_location
}

# To create a storage account
resource "azurerm_storage_account" "example" {
  name                     = "${var.az_prefix}${var.az_environment}${random_string.random.result}"
  resource_group_name      = azurerm_resource_group.example.name
  location                 = var.az_location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  public_network_access_enabled = true
}

# To create a container in the storage account
resource "azurerm_storage_container" "example" {
  for_each = toset(local.container_names)
  name                  = "${var.az_prefix}-${var.az_environment}-${each.value}"
  storage_account_name  = azurerm_storage_account.example.name
  container_access_type = "private"
}

# To create a blob in the container (Upload a file)
resource "azurerm_storage_blob" "example1" {
  for_each = toset(local.container_names)
  
  name                   = var.blob_name_1
  storage_account_name   = azurerm_storage_account.example.name
  storage_container_name = azurerm_storage_container.example[each.key].name
  type                   = "Block"
  source                 = var.blob_name_1_path
}

# To create a blob in the container (Upload a file)
resource "azurerm_storage_blob" "example2" {
  for_each = toset(local.container_names)

  name                   = var.blob_name_2
  storage_account_name   = azurerm_storage_account.example.name
  storage_container_name = azurerm_storage_container.example[each.key].name
  type                   = "Block"
  source                 = var.blob_name_2_path
}

# To create a file share
resource "azurerm_storage_share" "example" {
  name                 = "${var.az_prefix}${var.az_environment}${random_string.random.result}"
  storage_account_name = azurerm_storage_account.example.name
  quota                = 50

  acl {
    id = "MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTI"

    access_policy {
      permissions = "rwdl"
      start       = "2019-07-02T09:38:21.0000000Z"
      expiry      = "2019-07-02T10:38:21.0000000Z"
    }
  }
}