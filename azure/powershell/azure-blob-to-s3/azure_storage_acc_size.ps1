#############################
# Variables
#############################
$subscriptionid = "71e10ac9-a8a2-48aa-8db6-496b91fa7449"
$azStorageAccountsAll = @()

# Variables
$resourcegroupname = "Default-Storage-EastUS"
$storageaccountname = "vpc3uploadedfiles"
$containername = "thumbnails-test"
$totalsize = 0

#############################
# Script
#############################
# To set the current subscription 
$currsubscriptionid = (Get-AzContext).Subscription.id
if($currsubscriptionid -ne $subscriptionid)
{
    Clear-AzContext -Scope CurrentUser -Force
    Connect-AzAccount 
    Set-AzContext -Subscription $subscriptionid
    $currsubscriptionid = (Get-AzContext).Subscription.Name
    Write-Host "Subscription set to $currsubscriptionid ..."
}
else
{
    Write-Host "Subscription is already set to $currsubscriptionid ..."
}

# To get the list of Storage Accounts
If($storageaccountname -ne $null)
{
    $azStorageAccounts = Get-AzStorageAccount -StorageAccountName $storageaccountname -ResourceGroupName $resourcegroupname
}
else {
    $azStorageAccounts = Get-AzStorageAccount    
}

# To create an object with the Storage Account Details
foreach($azStorageAccount in $azStorageAccounts)
{
    # To get the account key
    $azStorageAccountKey = (Get-AzStorageAccountKey -ResourceGroupName $azStorageAccount.ResourceGroupName -Name $azStorageAccount.StorageAccountName | Select -First 1).value
    
    # Custom Object for Azure Storage Accounts
    $azStorageAccProperties = [ordered] @{
        accountRg        = $azStorageAccount.ResourceGroupName
        accountName      = $azStorageAccount.StorageAccountName
        accountKey       = $azStorageAccountKey
    }
    
    $azStorageAccounts = New-Object -Type PSObject -Property $azStorageAccProperties

    $azStorageAccountsAll += $azStorageAccounts
}

# $azStorageAccountsAll | Format-Table 

foreach($azStorageAcc in $azStorageAccountsAll)
{
    Write-Host "------------------------------------------" -ForegroundColor Yellow
    Write-Host "Storage Account: $($azStorageAcc.accountName)"
    Write-Host "------------------------------------------" -ForegroundColor Yellow
    $storageacclength = 0 

    $storageAcc = Get-AzStorageAccount -ResourceGroupName $azStorageAcc.accountRg -Name $azStorageAcc.accountName          
    $ctx = $storageAcc.Context    
    If($containername -ne $null)
    {
        $containers = $containername
    }
    else {
        $containers = (Get-AzStorageContainer  -Context $ctx).Name    
    }

    foreach($container in $containers)
    {
        # get a list of all of the blobs in the container 
        $listOfBlobs = Get-AzStorageBlob -Container $container -Context $ctx 
        $containerlength = 0

        $listOfBlobs | ForEach-Object {$containerlength = $containerlength + $_.Length}

        Write-Host "Container : $($container.Name) = " ($containerlength/1GB) "GB"

        $storageacclength = $storageacclength + $containerlength
        
    }
    Write-Host "------------------------------------------" -ForegroundColor Yellow
    Write-Host "StorageAccount : $($azStorageAcc.Name) = " ($storageacclength/1GB) "GB"
    $totalsize = $totalsize + $storageacclength
}


Write-Host "TotalSize : " ($totalsize/1GB) "GB"


