
$SubscriptionID = "7d042324-4517-404d-b79e-f148fc7f6f08"
$KeyvaultName = "aeu2-f08-o-ws-kv01"
$daystoExpire = 30


az account set --subscription $SubscriptionID

$certExpiryDate = (((((az keyvault certificate show --name le-wildcard --vault-name $KeyvaultName --query "attributes").split(",")[5]).Split(":")[1]).Split("T")[0]).replace("`"","")).trim()
$thresholdDate = ((Get-Date).AddDays($daystoexpire)).ToString("yyyy-MM-dd")

if($certExpiryDate -lt $thresholdDate)
{
    Write-Host "The certificate is expiring in $daystoExpire days."
}
else {
    Write-Host "The certificate is not expiring in $daystoExpire days."
}
