#!/usr/bin/env python3
import os
import sys

# Ensure that the entraid module is importable
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from entraid import entraid
from administration import administration
# from aiml import aiml

def main():
    # Azure AD app registration details
    tenant_id = os.getenv('ARM_TENANT_ID')
    client_id = os.getenv('ARM_CLIENT_ID')
    subscription_id = os.getenv('ARM_SUBSCRIPTION_ID')
    client_secret = os.getenv('ARM_CLIENT_SECRET')

    if len(sys.argv) != 4:
        print("Usage: entrypoint <module> <submodule> <function>")
        sys.exit(1)

    module_name = sys.argv[1]
    submodule_name = sys.argv[2]
    function_name = sys.argv[3]

    # Check the function to call
    if module_name == 'entraid':
        entraid.call_entraid_function(submodule_name, function_name, tenant_id, client_id, client_secret)
    if module_name == 'administration':
        administration.call_administration_function(submodule_name, function_name, tenant_id, client_id, client_secret, subscription_id)
    # if module_name == 'aiml':
    #     aiml.call_aiml_function(function_name, tenant_id, client_id, client_secret)
    else:
        print(f"Unknown function: {module_name}.{submodule_name}.{function_name}")

if __name__ == '__main__':
    main()
