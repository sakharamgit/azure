# Import the list function from the list module
from .list import list_function

def call_appregistrations_function(tenant_id, client_id, client_secret):
    authority = f"https://login.microsoftonline.com/{tenant_id}"
    scope = ["https://graph.microsoft.com/.default"]
    
    try:
        # Call the list function with arguments
        list_function(tenant_id, client_id, client_secret, authority, scope)
    except Exception as e:
        print(f"An error occurred: {e}")
