from msal import ConfidentialClientApplication
import os
import requests



def list_function(tenant_id, client_id, client_secret, authority, scope):
    
    # Create a confidential client application
    app = ConfidentialClientApplication(client_id, authority=authority, client_credential=client_secret)

    # Acquire a token for the app
    token_response = app.acquire_token_for_client(scopes=scope)

    # Use the token to call Microsoft Graph API and list app registrations
    graph_url = 'https://graph.microsoft.com/v1.0/applications'
    headers = {'Authorization': 'Bearer ' + token_response['access_token']}
    response = requests.get(graph_url, headers=headers)

    # Print the JSON response containing app registrations
    print(response.json())
    # apps_data = response.json()
    # # Convert the relevant part of the JSON response to a DataFrame
    # df = pd.DataFrame(apps_data['value'])
    # # Adjust column names based on the actual structure of your JSON
    # df = df[['displayName', 'appId', 'createdDateTime']]
    # df.columns = ['Name', 'App/Client ID', 'Created Date']

    # # Print the DataFrame as a table
    # print(df.to_string(index=False))
