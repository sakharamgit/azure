# python/entraid/entraid.py
from entraid.appregistrations import appregistrations
from entraid.users import users

def call_administration_function(submodule_name, function_name, tenant_id, client_id, client_secret, subscription_id):
    if submodule_name == "resourcegroup":
        if function_name == 'list':
            appregistrations.call_resourcegroup_function(tenant_id, client_id, client_secret, subscription_id)
        else:
            print(f"Function '{function_name}' not found in appregistrations")
    elif submodule_name == "users":
        if function_name == 'list':
            users.call_users_function(tenant_id, client_id, client_secret)
        else:
            print(f"Function '{function_name}' not found in users")
