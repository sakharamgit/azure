# Virtual Wide Area Network (VWAN)

## Overview

Microsoft Azure Virtual WAN (VWAN) is a networking service that provides optimized and automated branch connectivity to, and through, Azure. Virtual WAN enables customers to connect their branches to each other and to Azure using a combination of Site-to-Site VPN, ExpressRoute, and Point-to-Site User VPN, all with unified management and monitoring in Azure.

## Key Features

- **Centralized Connectivity**: VWAN acts as a central hub for connecting branch offices, datacenters, and remote users to Azure resources.
- **Automated Configuration**: Simplifies the setup of network connections with automated configurations.
- **Scalability**: Easily scale to connect hundreds of branches and remote users.
- **Unified Management**: Manage all network connections and policies from a single pane of glass.

## Components

### Virtual WAN Hub

The hub is a central component in a Virtual WAN architecture. It acts as a gateway for various types of network connections, including VPN, ExpressRoute, and Virtual Network connections.

### VPN Gateway

A VPN gateway enables secure, cross-premises connectivity between your Azure virtual networks and on-premises networks.

### ExpressRoute Gateway

ExpressRoute is a dedicated private connection from your on-premises network to Azure. It offers higher security, reliability, and speed compared to traditional internet connections.

### Virtual Network Connections

Connect your Azure Virtual Networks (VNets) to the VWAN hub, allowing seamless communication between different VNets.

### SD-WAN Integration

Azure VWAN integrates with SD-WAN devices to optimize branch-to-branch and branch-to-Azure connectivity.

## Setup and Configuration

### Step 1: Create a Virtual WAN

1. Navigate to the Azure portal.
2. Search for "Virtual WAN" and select "Create".
3. Configure the necessary settings such as resource group, location, and virtual WAN name.

### Step 2: Add Hubs

1. In the Virtual WAN resource, add hubs in the desired regions.
2. Configure the hub settings, including gateway configurations for VPN and ExpressRoute.

### Step 3: Connect VNets to the Hub

1. Navigate to the Virtual WAN resource.
2. Select the hub you created and choose "Virtual network connections".
3. Add and configure the VNets you want to connect to the hub.

### Step 4: Configure Site-to-Site VPN

1. In the Virtual WAN resource, navigate to "VPN sites".
2. Add the VPN sites you want to connect.
3. Configure the IP address and other settings for the on-premises VPN devices.

### Step 5: Enable Point-to-Site VPN

1. In the Virtual WAN hub, enable Point-to-Site VPN.
2. Configure the necessary client settings, such as VPN protocols and authentication methods.

## Benefits

- **Optimized Performance**: High-speed connectivity with low latency.
- **Enhanced Security**: Secure connections using IPsec and SSL.
- **Global Reach**: Connect branches and users across the globe.
- **Simplified Management**: Centralized management of network connections and policies.

## Typical Use Cases

- **Branch Connectivity**: Connecting multiple branch offices to each other and to Azure resources.
- **Remote User Access**: Enabling remote users to securely access Azure resources.
- **Hybrid Cloud Scenarios**: Integrating on-premises networks with Azure for hybrid cloud deployments.

## Additional Resources

- [Microsoft Learn: Secure your virtual networks](https://learn.microsoft.com/en-gb/training/modules/security-virtual-networks/6-virtual-wide-area-network)
- [Azure Virtual WAN Documentation](https://docs.microsoft.com/en-us/azure/virtual-wan/virtual-wan-about)
- [Azure Networking Overview](https://docs.microsoft.com/en-us/azure/architecture/example-scenario/infrastructure/enterprise-vwan)

---

*This README.md provides a high-level overview of Azure Virtual WAN based on the Microsoft Learn module. For detailed guidance and hands-on tutorials, refer to the official Microsoft documentation.*
