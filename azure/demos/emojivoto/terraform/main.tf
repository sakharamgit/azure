
# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}
resource "azurerm_kubernetes_cluster" "cloudovenaks" {
  name                = "cloudovenaks"
  location            = "${azurerm_resource_group.cloudoven.location}"
  resource_group_name = "${azurerm_resource_group.cloudoven.name}"
  dns_prefix          = "cloudovenaks"

  agent_pool_profile {
    name            = "default"
    count           = 1
    vm_size         = "Standard_D1_v2"
    os_type         = "Linux"
    os_disk_size_gb = 30
  }

  agent_pool_profile {
    name            = "pool2"
    count           = 1
    vm_size         = "Standard_D2_v2"
    os_type         = "Linux"
    os_disk_size_gb = 30
  }

  service_principal {
    client_id     = "98e758f8r-f734-034a-ac98-0404c500e010"
    client_secret = "Jk==3djk(efd31kla934-=="
  }

  tags = {
    Environment = "Production"
  }
}

output "client_certificate" {
  value = "${azurerm_kubernetes_cluster.cloudovenaks.kube_config.0.client_certificate}"
}

output "kube_config" {
  value = "${azurerm_kubernetes_cluster.cloudovenaks.kube_config_raw}"
}