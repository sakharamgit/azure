# Define variables if needed, e.g., for the group name
variable "group_names" {
  description = "List of Azure AD group names to create"
  type        = list(string)
  default     = ["Admin Group", "Developers Group", "Finance Team", "HR Team", "IT Support", "Marketing Team", "Sales Team", "Operations Team"]
}


