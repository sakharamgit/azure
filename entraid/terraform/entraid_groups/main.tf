
data "azuread_client_config" "current" {}


resource "azuread_group" "adgroups" {
  for_each         = toset(var.group_names)
  display_name     = each.value
  mail_nickname    = replace(lower(each.value), " ", "")
  security_enabled = true
}

# # To create a azuread group
# resource "azuread_group" "admin" {
#   display_name     = "administrations_group"
#   mail_nickname    = "Administrators Group"
#   security_enabled = true

#   owners = [
#     data.azuread_client_config.current.object_id
#   ]
#   # Adding Members to the group
#   members = [
#     data.azuread_user.cloudadmin.object_id,
#     /* more users */
#   ]
# }

# To create a dynamic azuread group
# resource "azuread_group" "dynamic" {
#   display_name     = "AllReadersGroup"
#   owners           = [data.azuread_client_config.current.object_id]
#   security_enabled = true
#   types            = ["DynamicMembership"]

#   dynamic_membership {
#     enabled = true
#     rule    = "user.department -eq \"Sales\""
#   }
# }