##################################################################################
# VARIABLES
##################################################################################

// GCP Variables
variable "gcp_project_name" {}
variable "gcp_region" {}
variable "gcp_zone" {}

// Generic Variables
variable "prefix" {
  type    = string
  default = "ss"
}
variable "environment" {
  type    = string
  default = "dev"
}

variable "project" {
  type    = string
  default = "wpmysql"  
}

resource "random_string" "suffix" {
  length  = 2
  upper   = false
  lower   = true
  special = false
}