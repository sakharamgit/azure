
# To create a google virtual private cloud
resource "google_compute_network" "vpc" {
  name                    = "${var.prefix}-${var.project}-vpc"
  auto_create_subnetworks = "false"
}

# To create a subnet in the above vpc
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.prefix}-${var.project}-subnet"
  region        = var.gcp_region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.10.0.0/24"
}