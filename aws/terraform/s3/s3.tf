locals {
  s3_info          = yamldecode(file("${path.module}/s3.yaml"))
  s3_bucket_name   = local.s3_info.s3_bucket_name
  sql_ec2_iam_role = local.s3_info.sql_ec2_iam_role
  intg_account_id  = local.s3_info.intg_account_id

  s3_tags = merge(
    { "environment" = local.environment },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}

# To create an S3 bucket for database backup
resource "aws_s3_bucket" "sql-db-backup" {
  bucket = local.s3_bucket_name

  versioning {
    enabled = true
  }

  tags = merge(
    {
      "environment" = local.environment
      "Name"        = local.s3_info.s3_bucket_name
      "purpose"     = "s3-cit-sql-backup"
    },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}


resource "aws_s3_bucket_policy" "cross-account-permission" {
  bucket = aws_s3_bucket.sql-db-backup.id

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : "arn:aws:iam::${local.intg_account_id}:role/${local.sql_ec2_iam_role}"
        },
        "Action" : [
          "s3:GetObject",
          "s3:PutObject",
          "s3:PutObjectAcl",
          "s3:DeleteObject"
        ],
        "Resource" : "arn:aws:s3:::${aws_s3_bucket.sql-db-backup.bucket}/*"
      },
      {
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : "arn:aws:iam::${local.intg_account_id}:role/${local.sql_ec2_iam_role}"
        },
        "Action" : "s3:ListBucket",
        "Resource" : "arn:aws:s3:::${aws_s3_bucket.sql-db-backup.bucket}"
      }
    ]
  })
}