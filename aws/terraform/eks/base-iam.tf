# Attach the Systems Manager role for SSH accesss on the worker nodes
module "ec2_systems_manager" {
  source  = "app.terraform.io/cloudutsuk/iam/aws//modules/ec2_sessions_manager"
  version = "2.0.0"
}

resource "aws_iam_policy" "ec2_systems_manager" {
  name_prefix = "eks-workers-systems-manager-"
  policy      = module.ec2_systems_manager.policy
}

resource "aws_iam_role_policy_attachment" "workers_systems_manager" {
  policy_arn = aws_iam_policy.ec2_systems_manager.arn
  role       = module.eks.worker_iam_role_name
}

# Attach policy for DataDog metrics support
module "ec2_metadata" {
  source  = "app.terraform.io/cloudutsuk/iam/aws//modules/ec2_metadata"
  version = "2.0.0"
}

resource "aws_iam_policy" "ec2_metadata" {
  name_prefix = "eks-workers-ec2-metadata-"
  policy      = module.ec2_metadata.policy
}

resource "aws_iam_role_policy_attachment" "workers_ec2_metadata" {
  policy_arn = aws_iam_policy.ec2_metadata.arn
  role       = module.eks.worker_iam_role_name
}

module "eks_autoscaler" {
  source     = "app.terraform.io/cloudutsuk/iam/aws//modules/eks_autoscaling"
  version    = "2.0.0"
  cluster_id = module.eks.cluster_id
}

resource "aws_iam_policy" "eks_autoscaler" {
  name_prefix = "eks-workers-eks-autoscaler-"
  policy      = module.eks_autoscaler.policy
}

resource "aws_iam_role_policy_attachment" "workers_eks_autoscaler" {
  policy_arn = aws_iam_policy.eks_autoscaler.arn
  role       = module.eks.worker_iam_role_name
}

