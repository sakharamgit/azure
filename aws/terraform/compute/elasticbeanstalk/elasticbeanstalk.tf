
# Create an IAM role for Elastic Beanstalk
resource "aws_iam_role" "instance_role" {
  name = "my-ml-eb-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect    = "Allow",
      Principal = {
        Service = "ec2.amazonaws.com"
      },
      Action    = "sts:AssumeRole"
    }]
  })
}

# Create an IAM instance profile for Elastic Beanstalk
resource "aws_iam_instance_profile" "instance_profile" {
  name = "my-ml-eb-instance-profile"
  role = aws_iam_role.instance_role.name
}

# Attach policies to the IAM role (adjust policies as per your requirements)
resource "aws_iam_role_policy_attachment" "instance_role_attachment" {
  role       = aws_iam_role.instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

# Create Elastic Beanstalk Application
resource "aws_elastic_beanstalk_application" "example" {
  name        = "${var.prefix}-${var.project}-${var.env}-eb-app"
  description = "Elastic Beanstalk Application"
}

# Create Elastic Beanstalk Application Version
resource "aws_elastic_beanstalk_application_version" "app_version" {
  name        = var.appversion
  application = aws_elastic_beanstalk_application.example.name
  description = "Version 1 of my ML application"
  bucket      = aws_s3_bucket.app_bucket.bucket
  key         = aws_s3_bucket_object.app_zip.key

  depends_on = [aws_s3_bucket_object.app_zip]
}

# Create Elastic Beanstalk Environment
resource "aws_elastic_beanstalk_environment" "env" {
  name                = "${var.prefix}-${var.project}-${var.env}-eb-env"
  application         = aws_elastic_beanstalk_application.example.name
  solution_stack_name = var.solution_stack_name
  version_label       = aws_elastic_beanstalk_application_version.app_version.name

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = "t2.micro"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "EnvironmentType"
    value     = "SingleInstance"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "HealthCheckPath"
    value     = "/"
  }

   setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = aws_iam_instance_profile.instance_profile.name
  }

  tags = {
    Name        = var.env
    Environment = var.env
  }
}

# Create an S3 bucket
resource "aws_s3_bucket" "app_bucket" {
  bucket = "${var.prefix}-${var.project}-${var.env}-s3"
}

# Upload the application zip file to the S3 bucket
resource "aws_s3_bucket_object" "app_zip" {
  depends_on = [ aws_s3_bucket.app_bucket ]
  bucket = aws_s3_bucket.app_bucket.bucket
  key    = var.s3_file_path
  source = "./sampleapp/pythonapp.zip"
}