# AWS Elastic Beanstalk with Terraform

- This repository contains Terraform code to deploy an AWS Elastic Beanstalk App. 
- Elastic Beanstalk is a service for deploying and scaling web applications and services. 
- Upload your code and Elastic Beanstalk automatically handles the deployment—from capacity provisioning, load balancing, and auto scaling to application health monitoring.

## Prerequisites

Before you begin, ensure you have the following:

1. [Terraform](https://www.terraform.io/downloads.html) installed on your local machine.
2. [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) installed and authenticated.
3. An AWS Account. 

## Getting Started

### Clone the Repository

```sh
git clone https://gitlab.com/sakharamgit/cloud.git
cd aws/terraform/computer/elasticbeanstalk
```

### Configure Terraform Backend (Optional)
If you want to configure a remote backend for Terraform state, update the backend.tf file with your backend configuration.

### Initialize Terraform
- Initialize the Terraform configuration:

```sh
terraform init
```

### Review and Apply the Configuration
- Review the Terraform plan to see what resources will be created:

```sh
terraform plan
```

- Apply the Terraform plan to create the resources:

```sh
terraform apply
```
