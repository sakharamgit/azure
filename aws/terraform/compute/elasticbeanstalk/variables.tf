##################################################################################
# VARIABLES
##################################################################################

# Common Variables
variable "prefix" {
  type        = string
  default     = "ss"
  description = "Value to be prefixed to all resources"
}

variable "project" {
  type        = string
  default     = "cb"
  description = "Name of the project"
}

variable "env" {
  type        = string
  default     = "dev"
  description = "Name of the environment"
}


variable "appversion" {
  type        = string
  default     = "v1"
  description = "The version of the application"
}

variable "s3_file_path" {
  type        = string
  default     = "pythonapp.zip"
  description = "The path to the application zip file"
}

variable "solution_stack_name" {
  type        = string
  default     = "64bit Amazon Linux 2 v3.7.0 running Python 3.8"
  description = "The solution stack name"
}