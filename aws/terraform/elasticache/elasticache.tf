
locals {
  ec_info                        = yamldecode(file("${path.module}/elasticache.yaml"))
  engine                         = local.ec_info.engine
  engine_version                 = local.ec_info.engine_version
  number_cache_clusters          = local.ec_info.number_cache_clusters
  node_type                      = local.ec_info.node_type
  parameter_group_name           = local.ec_info.parameter_group_name
  replication_group_description  = local.ec_info.replication_group_description
  elasticache_subnet_name        = local.ec_info.elasticache_subnet_name
  elasticache_availability_zones = local.ec_info.elasticache_availability_zones
  service_name                   = local.ec_info.service_name
  iam_role                       = local.ec_info.iam_role
  create_custom_security_group   = lookup(local.ec_info, "redis_custom_security_group", false)
  access_over_vpn                = lookup(local.ec_info, "access_over_vpn", false)
  access_from_kubernetes_vpc     = lookup(local.ec_info, "access_from_kubernetes_vpc", false)
}

module "elasticache_name" {
  source      = "app.terraform.io/cloudutsuk/name/aws"
  version     = "12.0.0"
  environment = local.environment
  resource    = "rediscache"
}

# To create AWS IAM Role for  elasticache instances

resource "aws_iam_role" "elasticache_iam_role" {
  name = local.iam_role
  path = "/"
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Principal" : {
            "Federated" : module.eks.oidc_provider_arn
          },
          "Action" : "sts:AssumeRoleWithWebIdentity",
          "Condition" : {
            "StringEquals" : {
              "${replace(module.eks.cluster_oidc_issuer_url, "https://", "")}:sub" : "system:serviceaccount:cit:cit-vpc-api"
            }
          }
        }
      ]
  })
  inline_policy {
    name = "elasticache_iam_policy"
    policy = jsonencode({
      "Version" = "2012-10-17",
      "Statement" = [
        {
          "Sid"    = "EksElasticachePermission",
          "Effect" = "Allow",
          "Action" = [
            "elasticache:RemoveTagsFromResource",
            "elasticache:DescribeCacheParameters",
            "elasticache:ModifyCacheParameterGroup",
            "elasticache:DescribeCacheParameterGroups",
            "elasticache:CreateCacheParameterGroup",
            "elasticache:AddTagsToResource",
            "elasticache:DeleteCacheParameterGroup",
            "elasticache:DescribeCacheClusters",
            "elasticache:ListTagsForResource"
          ],
          "Resource" = "*"
        }
      ]
    })
  }
}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = [module.data_store_vpc[0].vpc_name]
  }
}


# To get the subnet details for elasticache instances
data "aws_subnet" "elasticache_subnets" {
  count             = length(local.elasticache_availability_zones)
  availability_zone = local.elasticache_availability_zones[count.index]
  filter {
    name   = "tag:Name"
    values = [format("%s-%s-*", module.data_store_vpc[0].vpc_name, local.elasticache_subnet_name)]
  }
}

resource "aws_elasticache_subnet_group" "redis_subnet" {
  name       = "elasticache-subnets-group"
  subnet_ids = data.aws_subnet.elasticache_subnets.*.id
}

resource "aws_security_group" "redis_custom_sg" {
  count = local.create_custom_security_group ? 1 : 0
  name  = "${module.elasticache_name.name}-default"
  tags = merge(
    {
      "environment"  = local.environment
      "service_name" = local.service_name
    },
    module.aws_global.tags
  )
}

resource "aws_security_group_rule" "access_from_kubernetes_vpc_sgr" {
  count             = local.create_custom_security_group && local.access_from_kubernetes_vpc ? 1 : 0
  type              = "ingress"
  from_port         = module.elasticache.port
  to_port           = module.elasticache.port
  protocol          = "TCP"
  cidr_blocks       = [module.kubernetes_vpc.vpc_cidr_block]
  security_group_id = aws_security_group.redis_custom_sg[0].id
}

resource "aws_security_group_rule" "access_over_vpn_sgr" {
  count             = local.create_custom_security_group && local.access_over_vpn ? 1 : 0
  type              = "ingress"
  from_port         = module.elasticache.port
  to_port           = module.elasticache.port
  protocol          = "TCP"
  cidr_blocks       = ["${module.aws_global.vpn_vpcs["production-vpn-engops-us-west-2"]}/${module.aws_global.vpn_vpcs["vpn_vpc_cidr_block_size"]}"]
  security_group_id = aws_security_group.redis_custom_sg[0].id
}

module "elasticache" {
  source                        = "app.terraform.io/cloudutsuk/elasticache/aws"
  version                       = "13.0.0"
  engine                        = local.engine
  engine_version                = local.engine_version
  environment                   = local.environment
  service_name                  = local.service_name
  subnet_group_name             = aws_elasticache_subnet_group.redis_subnet.name
  number_cache_clusters         = local.number_cache_clusters
  node_type                     = local.node_type
  parameter_group_name          = local.parameter_group_name
  replication_group_description = local.replication_group_description
  security_group_ids            = local.create_custom_security_group ? [aws_security_group.redis_custom_sg[0].id] : []
}
