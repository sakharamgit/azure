# Locals for NLB for VPC
locals {
  nlb_info       = yamldecode(file("${path.module}/nlb.yaml"))
  subnet_name    = local.nlb_info.subnet_name
  ports          = local.nlb_info.ports
  nlb_az         = local.nlb_info.availability_zone
  ec2_private_ip = local.nlb_info.ec2_private_ip
  use_static_ip  = try(local.nlb_info.use_static_ip, false)

  nlb_tags = merge(
    { "environment" = local.environment },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}

output "nlb_ip" {
  description = "The IP of the NLB (could be private or public depending on use_static_ip)"
  # Assume there is at least one target group; they all share the same IP.
  value = aws_lb_target_group_attachment.tga[0].target_id
}

data "aws_vpc" "this" {
  filter {
    name   = "tag:Name"
    values = [module.custom_vpc.name]
  }
}

data "aws_subnet" "this" {
  availability_zone = local.nlb_az
  filter {
    name   = "tag:Name"
    values = [format("%s-%s-*", module.custom_vpc.name, local.subnet_name)]
  }
}

# Create a name for the NLB
module "nlb_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "nlb"
}

resource "aws_eip" "this" {
  count = local.use_static_ip ? 1 : 0
  vpc   = true
  tags  = local.nlb_tags
}

module "targetgroup_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "tg"
}

# To create a network load balancer (NLB)
resource "aws_lb" "nlb" {
  name               = module.nlb_name.name
  internal           = false
  load_balancer_type = "network"

  subnet_mapping {
    subnet_id     = data.aws_subnet.this.id
    allocation_id = local.use_static_ip ? aws_eip.this[0].id : null
  }

  enable_deletion_protection       = false
  enable_cross_zone_load_balancing = true

  tags = local.nlb_tags
}

# To create the target group and attach it to the NLB
resource "aws_lb_target_group" "nlbtargetgroup" {
  count       = length(local.ports)
  name        = "${module.targetgroup_name.name}-${local.ports[count.index]}"
  port        = local.ports[count.index]
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.this.id
}

# To add the targets to the target group
resource "aws_lb_target_group_attachment" "tga" {
  count            = length(local.ports)
  target_group_arn = aws_lb_target_group.nlbtargetgroup[count.index].arn
  port             = local.ports[count.index]
  target_id        = local.ec2_private_ip
}

# To create a NLB listener
resource "aws_lb_listener" "lblist" {
  count = length(local.ports)

  load_balancer_arn = aws_lb.nlb.arn

  port     = local.ports[count.index]
  protocol = "TCP"
  default_action {
    target_group_arn = aws_lb_target_group.nlbtargetgroup[count.index].arn
    type             = "forward"
  }
}
