

<powershell>

# To set the TLS version to 1.2
Set-ExecutionPolicy Bypass -Scope Process -Force; 
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; 


# Creating Directories for Prometheus
New-Item C:\files -ItemType Directory  -Force | Out-Null
"Initial Directories Created" | Out-File C:\files\initial_config.txt -Append

# To add the user account
net user ${windows_username} /add /y

# To set the password
net user ${windows_username} ${windows_password}

# To add the user to administrator group
net localgroup administrators ${windows_username} /add
"User created and added to Administrators group" | Out-File C:\files\initial_config.txt -Append

# To enable WinRM
winrm quickconfig -q
winrm set winrm/config/service/auth '@{Basic="true"}'
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="300"}'
winrm set winrm/config '@{MaxTimeoutms="1800000"}'
"WinRM configured with basic settings" | Out-File C:\files\initial_config.txt -Append

# To add the firewall rule 
netsh advfirewall firewall add rule name="WinRM 5985" protocol=TCP dir=in localport=5985 action=allow
netsh advfirewall firewall add rule name="WinRM 5986" protocol=TCP dir=in localport=5986 action=allow
netsh advfirewall firewall add rule name="Prometheus 9090" protocol=TCP dir=in localport=9090 action=allow
netsh advfirewall firewall add rule name="Grafana 3000" protocol=TCP dir=in localport=3000 action=allow
"Firewall rules were added for WinRM and Prometheus" | Out-File C:\files\initial_config.txt -Append

# To set the winrm service to auto start
net stop winrm
sc.exe config winrm start=auto
net start winrm
"WinRM service was restarted and set to Automatic" | Out-File C:\files\initial_config.txt -Append

# To allow remote powershell
Set-Item WSMan:\localhost\client\Trustedhosts -Value * -Force
Restart-Service WinRM -Force
"WinRM configured to allow connections from all hosts" | Out-File C:\files\initial_config.txt -Append

</powershell>
