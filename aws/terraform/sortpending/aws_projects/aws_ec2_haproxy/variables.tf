##################################################################################
# VARIABLES
##################################################################################

//AWS Variables
variable "aws_key_name" {}
variable "private_key_path" {}
variable "aws_username" {}
variable "aws_region" {}
variable "OG_AWS_ACCESS_KEY_ID" {}
variable "OG_AWS_SECRET_ACCESS_KEY" {}


variable "db_vpc_id" {}
variable "pub_uw2a_subnet_id" {}
variable "sample_txt_file_path" {}