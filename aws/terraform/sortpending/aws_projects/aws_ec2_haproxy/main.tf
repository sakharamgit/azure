// To retrieve the latest AMI for Ubuntu
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

// To retrieve VPC 
data "aws_vpc" "db_vpc" {
  id = var.db_vpc_id
}

// To retrieve public subnet
data "aws_subnet" "db_pub_sub_usw2a" {
  id = var.pub_uw2a_subnet_id
}

// To create a network interface
resource "aws_network_interface" "example" {
  subnet_id = data.aws_subnet.db_pub_sub_usw2a.id

  tags = {
    Name = "haproxy_nic"
  }
}

// To create a security group
resource "aws_security_group" "sg_haproxy" {
  name        = "sg_haproxy"
  description = "AWS Security Group for HAProxy EC2"
  vpc_id      = data.aws_vpc.db_vpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg_haproxy"
  }
}

// To create Amazon EC2 instance
resource "aws_instance" "ha_proxy_ec2" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  //user_data              = data.template_file.ubuntu.template
  key_name                    = var.aws_key_name
  subnet_id                   = data.aws_subnet.db_pub_sub_usw2a.id
  vpc_security_group_ids      = [aws_security_group.sg_haproxy.id]
  associate_public_ip_address = true


  tags = {
    Name = "haproxy-aws-ec2"
  }
}

resource "null_resource" "file_copy" {
  depends_on = [aws_instance.ha_proxy_ec2]

  connection {
    type        = "ssh"
    host        = aws_instance.ha_proxy_ec2.public_ip
    user        = var.aws_username
    private_key = file(var.private_key_path)
  }

  provisioner "file" {
    source      = var.sample_txt_file_path
    destination = "/tmp/sample_txt_file.txt"
  }

  

}