variable "aws_region" {
  type    = string
  default = "us-west-2"
}

variable "OG_AWS_ACCESS_KEY_ID" {
  type = string
}

variable "OG_AWS_SECRET_ACCESS_KEY" {
  type = string
}

variable "sql_availability_zones" {
  default = ["us-west-2a", "us-west-2b"]
}