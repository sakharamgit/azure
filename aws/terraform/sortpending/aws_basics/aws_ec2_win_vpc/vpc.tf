
// To create a VPC
resource "aws_vpc" "example" {
  cidr_block           = var.vpc_cidr_range
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-vpc"
    environment = var.env
  }
}

# Internet Gateway
resource "aws_internet_gateway" "example" {
  vpc_id = aws_vpc.example.id
  tags = {
    Name = "${var.prefix}-${var.project}-${var.env}-ig"
  }
}


# To create a route table
resource "aws_route_table" "example" {
  vpc_id = aws_vpc.example.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example.id
  }

  tags = {
    Name = "${var.prefix}-${var.project}-${var.env}-routetable"
  }
}


// To create a subnet in the VPC
resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.aws_availability_zone

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-subnet"
    environment = var.env
  }
}

# To create a route table association
resource "aws_route_table_association" "example1" {
  subnet_id      = aws_subnet.example.id
  route_table_id = aws_route_table.example.id
}

// To create a network interface
resource "aws_network_interface" "example" {
  subnet_id   = aws_subnet.example.id
  private_ips = var.nic_private_ip

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-aws-ec2-nic"
    environment = var.env
  }
}
