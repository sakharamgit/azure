﻿$alwaysonNodes = "W16Node3","W16Node1"
$alwaysonGroup = "AG_Group1"
$userAccountAG = "sakharam\administrator"
$agDatabase = "AG_TestDB"
$agListenerName = "AG_Group1_List"
$agListenerIP = "193.168.10.38"
$alwaysonGroupEndpoint = $alwaysonGroup + "_Endpoint"

foreach($alwaysonNode in $alwaysonNodes)
{
    # To install the SQL PS Module
    $sqlpsModuleInstallStatus = Invoke-Command -ComputerName $alwaysonNode -ScriptBlock { 
                                                                                        (Get-Module -ListAvailable | Where-Object {$_.Name -eq 'SQLPS'}).Name
                                                                                    } -Verbose
    If(!$sqlpsModuleInstallStatus)
    {
        Invoke-Command -ComputerName $alwaysonNode -ScriptBlock { Install-Module -Name SqlServer -Force }
    }
    else
    {
        Write-Host "SQLPS module is already installed on the serer $alwaysonNode." -ForegroundColor Yellow
    }

    # To import the SQL module
    $sqlpsModuleImportStatus = Invoke-Command -ComputerName $alwaysonNode -ScriptBlock { 
                                                                                (Get-Module -ListAvailable | Where-Object {$_.Name -eq 'SQLPS'} | Select -First 1).Name
                                                                             } -Verbose
    If($sqlpsModuleImportStatus)
    {
        Invoke-Command -ComputerName $alwaysonNode -ScriptBlock { Import-Module -Name SQLPS }
        Write-Host "SQLPS module imported on the serer $alwaysonNode successfully." -ForegroundColor Yellow
    }
    else
    {
        Write-Host "SQLPS module is already imported on the serer $alwaysonNode." -ForegroundColor Yellow
    }

    # To set the SQL Provide location
    $sqlProviderLocation = Invoke-Command -ComputerName $alwaysonNode -ScriptBlock {
                                                                                    try
                                                                                    {
                                                                                        $result = Test-Path "SQLServer:\SQL" 
                                                                                        If($result -eq 'True') { Return 1 }
                                                                                    }
                                                                                    catch
                                                                                    {
                                                                                        Return "$_.Exception.Message"
                                                                                    }
                                                                                } 
    Switch($sqlProviderLocation)
    {
        "1"  { "Location Successfully set to SQL Provider." }
        default { $sqlProviderLocation }
    }

    # To enable Always On
    Write-host "Checking Always On..."
    $isHADREnabled = @()
    $isHadrEnabled = Invoke-Command -ComputerName $alwaysonNode -ScriptBlock { 
                                                                            param($alwaysonNode)
                                                                            Import-Module SQLPS 
                                                                            (Get-Item -Path "SQLSERVER:\SQL\$alwaysonNode\Default\" | Select IsHadrEnabled).IsHadrEnabled
                                                                          } -ArgumentList $alwaysonNode -Verbose

    If($isHadrEnabled -eq 'True')
    {
        Write-Host "Always On is already enabled on the SQL instance $alwaysonNode." -ForegroundColor Yellow
    }
    else
    {
        Invoke-Command -ComputerName $alwaysonNode -ScriptBlock { 
                                                                param($alwaysonNode)
                                                                Enable-SqlAlwaysOn -Path "SQLSERVER:\SQL\$alwaysonNode\Default" -Force
                                                            } -ArgumentList $alwaysonNode
        Write-Host "Always On is enabled on the SQL instance $alwaysonNode successfully." -ForegroundColor Green
        $isHadrEnabled = $true
    }

    If($isHADREnabled -eq "True")
    {
        #Creating AlwaysOn Group
        Invoke-Command -ComputerName $alwaysonNode -ScriptBlock {
                                                                    param($alwaysonNode,$alwaysonGroup,$alwaysonGroupEndpoint,$userAccountAG)
                                                                    $alwaysonGroupEndpointPresent = @()
                                                                    $userAccountAGPresent = @()

                                                                    Import-Module sqlps

                                                                    # To check and create AG enpoint on the instance
                                                                    If(((Get-Item -Path "SQLSERVER:\SQL\$alwaysonNode\Default\Endpoints").Collection).Name -contains $alwaysonGroupEndpoint)
                                                                    {
                                                                        Write-host "The endpoint $alwaysonGroupEndpoint is already created on the server $alwaysonNode." -ForegroundColor Yellow
                                                                        $alwaysonGroupEndpointPresent = $true
                                                                    }
                                                                    else
                                                                    {
                                                                        New-SqlHADREndpoint -Path "SQLSERVER:\SQL\$alwaysonNode\Default" -Name "$alwaysonGroupEndpoint" -Port 5022 -EncryptionAlgorithm AES -Encryption Required 
                                                                        Write-host "The endpoint $alwaysonGroupEndpoint is created successfully on the server $alwaysonNode." -ForegroundColor Green
                                                                        $alwaysonGroupEndpointPresent = $true
                                                                    }

                                                                    # To start the AG endpoint
                                                                    $agEndpointStateQuery = @"
                                                                    SELECT state_desc FROM sys.tcp_endpoints where name = '$alwaysonGroupEndpoint'
"@
                                                                    If((Invoke-Sqlcmd -ServerInstance $alwaysonNode -Query $agEndpointStateQuery).state_desc -eq 'Stopped')
                                                                    {
                                                                        Set-SqlHADREndpoint -Path "SQLSERVER:\SQL\$alwaysonNode\Default\Endpoints\$alwaysonGroupEndpoint" -State Started
                                                                        Write-Host "The Always On endpoint $alwaysonGroupEndpoint has been started." -ForegroundColor Green
                                                                    }
                                                                    else
                                                                    {
                                                                        Write-Host "The Always On endpoint $alwaysonGroupEndpoint is currently in running state. " -ForegroundColor Yellow
                                                                    }

                                                                    # To check and create user account on the instance
                                                                    If((Invoke-Sqlcmd -ServerInstance $alwaysonNode -Query "SELECT name FROM syslogins where name = '$userAccountAG'").Name -eq $userAccountAG)
                                                                    {
                                                                        Write-Host "The user account $userAccountAG is already present on the server $alwaysonNode." -ForegroundColor Yellow
                                                                        $userAccountAGPresent = $true
                                                                    }
                                                                    else
                                                                    {
                                                                        Invoke-Sqlcmd -ServerInstance $alwaysonNode -Query "CREATE LOGIN [$userAccountAG] FROM WINDOWS;"
                                                                        Write-Host "The user account $userAccountAG created successfully on the server $alwaysonNode." -ForegroundColor Green
                                                                        $userAccountAGPresent = $true
                                                                    }

                                                                    # To grant Connect permissions on the AG enpoint
                                                                    If($alwaysonGroupEndpointPresent -eq "True")
                                                                    {
                                                                        If($userAccountAGPresent -eq "True")
                                                                        {
                                                                            Invoke-Sqlcmd -ServerInstance $alwaysonNode -Query "GRANT CONNECT ON ENDPOINT::$alwaysonGroupEndpoint TO [$userAccountAG];"
                                                                            Write-Host "Granted Connect Permissions to the user $userAccountAG on the Always On endpoint $alwaysonGroupEndpoint" -ForegroundColor Green
                                                                        }
                                                                        else
                                                                        {
                                                                            Write-Host "The user account $userAccountAG is not present on the server $alwaysonNode." -ForegroundColor Red
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        Write-Host "The always on endpint $alwaysonGroupEndpoint is not present on the server $alwaysonNode." -ForegroundColor Red
                                                                    }
                                                                } -ArgumentList $alwaysonNode,$alwaysonGroup,$alwaysonGroupEndpoint,$userAccountAG -Verbose
    }
    
}


# Setting up databases for Always On
$primaryNode = $alwaysonNodes[0]
$secondaryNode = $alwaysonNodes[1]


Invoke-Command -ComputerName $primaryNode -ScriptBlock {
                                                            param($primaryNode)
                                                            If(!(Test-Path C:\SQLBackup))
                                                            {
                                                                New-Item C:\SQLBackup -ItemType Directory
                                                                Write-Host "The directory C:\SQLBackup created successfully on $primaryNode." -ForegroundColor Green
                                                            }
                                                            else
                                                            {
                                                                Write-Host "The directory C:\SQLBackup exists on $primaryNode." -ForegroundColor Green
                                                            }
                                                            If(!(Get-SmbShare -Name SQLBackup))
                                                            {
                                                                New-SMBShare –Name “SQLBackup” –Path “C:\SQLBackup” –ContinuouslyAvailable –FullAccess domain\administrators -ChangeAccess domain\administrators -ReadAccess “domain\authenticated users"

                                                            }
                                                            else
                                                            {
                                                               Write-Host "The network share SQLbackup is already present on $primaryNode." -ForegroundColor Yellow 
                                                            }
                                                        } -ArgumentList $primaryNode -Verbose

Invoke-Command -ComputerName $secondaryNode -ScriptBlock  {
                                                                param($secondaryNode)
                                                                If(!(Test-Path C:\SQLRestore))
                                                                {
                                                                    New-Item C:\SQLRestore -ItemType Directory
                                                                    Write-Host "The directory C:\SQLRestore created successfully on $secondaryNode." -ForegroundColor Green
                                                                }
                                                                else
                                                                {
                                                                    Write-Host "The directory C:\SQLRestore exists on $secondaryNode." -ForegroundColor Green
                                                                }
                                                                If(!(Get-SmbShare -Name SQLRestore))
                                                                {
                                                                    New-SMBShare –Name “SQLRestore” –Path “C:\SQLRestore” –ContinuouslyAvailable –FullAccess domain\administrators -ChangeAccess domain\administrators -ReadAccess “domain\authenticated users"
                                                                    Write-Host "The network share SQLRestore is created successfully on $secondaryNode." -ForegroundColor Green
                                                                }
                                                                else
                                                                {
                                                                    Write-Host "The network share SQLRestore is already present on $secondaryNode." -ForegroundColor Yellow
                                                                }
                                                           } -ArgumentList $secondaryNode -Verbose

# Copying files from primary to secondary node
Remove-Item -Path "\\$primaryNode\SQLBackup\*" 

# Restoring database on secondary instance
Invoke-Command -ComputerName $primaryNode -ScriptBlock {
                                                                        If(!(Test-Path C:\SQLRestore))
                                                                        {
                                                                            $agDatabasePrereqQuery= @"
                                                                            DECLARE @dbname nvarchar(128)
                                                                            SET @dbname = N'AG_TestDB'
                                                                            IF (EXISTS(SELECT name FROM master.dbo.sysdatabases WHERE name = @dbname))
                                                                            Begin
                                                                            Print 'Database is already present on the instance'
                                                                            End
                                                                            ELSE
                                                                            Create Database AG_TestDB 
                                                                            Go
                                                                            Backup Database AG_TestDB  To Disk='C:\SQLBackup\AG_TestDB.bak'
                                                                            Go
                                                                            Backup Log AG_TestDB  To Disk='C:\SQLBackup\AG_TestDB.trn'
                                                                            Go
"@
                                                                            Invoke-Sqlcmd -ServerInstance $primaryNode -Query $agDatabasePrereqQuery
                                                                        }
                                                        }

# Copying files from primary to secondary node
Copy-Item -Path "\\$primaryNode\SQLBackup\*" -Destination "\\$secondaryNode\SQLRestore"

Invoke-Command -ComputerName $secondaryNode -ScriptBlock  {
                                                                        $agDatabasePrereqQuery= @"
                                                                        DECLARE @dbname nvarchar(128)
                                                                        DECLARE @sqlcmd nvarchar(200)
                                                                        SET @dbname = N'AG_TestDB'
                                                                        SET @sqlcmd = 'DROP DATABASE ' + @dbname	

                                                                        IF (EXISTS(SELECT name FROM master.dbo.sysdatabases WHERE name = @dbname))
                                                                        Begin
                                                                        exec sp_executesql @sqlcmd
                                                                        End
                                                                        RESTORE DATABASE AG_TestDB FROM DISK = 'C:\SQLRestore\AG_TestDB.bak' WITH NORECOVERY
                                                                        GO
                                                                        RESTORE DATABASE AG_TestDB FROM DISK = 'C:\SQLRestore\AG_TestDB.trn' WITH NORECOVERY
                                                                        GO
"@
                                                                        Invoke-Sqlcmd -ServerInstance $secondaryNode -Query $agDatabasePrereqQuery
                                                          }

# Creating the availability group 

Invoke-Command -ComputerName $primaryNode -ScriptBlock {
                                                                        param($primaryNode, $secondaryNode, $alwaysonNodes, $alwaysonGroup,$agListenerName,$agListenerIP)
                                                                        Import-Module sqlps
                                                                        
                                                                        If((Invoke-Sqlcmd -ServerInstance $primaryNode -Query "select Name from sys.availability_groups").Name -eq $alwaysonGroup)
                                                                        {
                                                                            Write-Host "The Always On group is already present on the server $primaryNode." -ForegroundColor Yellow
                                                                        }
                                                                        else
                                                                        {
                                                                            $agVersion = ((Get-Item "SQLSERVER:\Sql\$primaryNode\Default").version).Major
                                                                            $alwaysonNodeDomain = $env:userdnsdomain
                                                                            $primaryReplica = New-SqlAvailabilityReplica -Name "$primaryNode" -EndpointUrl ("TCP://$primaryNode" + "." + "$alwaysonNodeDomain" + ":5022") -FailoverMode "Automatic" -AvailabilityMode "SynchronousCommit" -AsTemplate -Version "$agVersion"
                                                                            $SecondaryReplica = New-SqlAvailabilityReplica -Name "$secondaryNode" -EndpointUrl ("TCP://$secondaryNode" + "." + "$alwaysonNodeDomain" + ":5022") -FailoverMode "Automatic" -AvailabilityMode "SynchronousCommit" -AsTemplate -Version "$agVersion"
                                                                            New-SqlAvailabilityGroup -InputObject $primaryNode -Name $alwaysonGroup -AvailabilityReplica ($primaryReplica,$SecondaryReplica) -Database $agDatabase
                                                                            Write-Host "The Always On group $alwaysonGroup is created sucessfully." -ForegroundColor Green

                                                                            Join-SqlAvailabilityGroup -Path “SQLSERVER:\SQL\$primaryNode\Default” -Name $alwayonGroup
                                                                            Add-SqlAvailabilityDatabase -Path "SQLSERVER:\SQL\$secondaryNode\Default\AvailabilityGroups\$alwayonGroup" -Database $agDatabase
                                                                            New-SqlAvailabilityGroupListener -Name $agListenerName -staticIP "$agListenerIP/255.255.255.0" -Port 1433 -Path "SQLSERVER:\SQL\$primaryNode\DEFAULT\AvailabilityGroups\$alwaysonGroup"
                                                                        }
                                                        } -ArgumentList $primaryNode,$secondaryNode,$alwaysonNodes,$alwaysonGroup,$agListenerName,$agListenerIP -Verbose


