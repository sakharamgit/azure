//To create a network load balancer
resource "aws_lb" "example" {
  name               = "${var.prefix}-${var.project}-${var.env}-nlb"
  internal           = false
  load_balancer_type = "network"
  subnets            = [aws_subnet.az1.id, aws_subnet.az2.id]

  enable_deletion_protection       = false
  enable_cross_zone_load_balancing = true

  tags = {
    Environment = var.env
  }
}

// To create the target group and attach it to the NLB
resource "aws_lb_target_group" "example" {
  name        = "${var.prefix}-${var.project}-${var.env}-nlb-tg"
  port        = 1433
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = aws_vpc.example.id
}

// To add the targets to the target group
resource "aws_lb_target_group_attachment" "tga1" {
  target_group_arn = aws_lb_target_group.example.arn
  port             = 1433
  target_id        = "10.0.1.15"
}

resource "aws_lb_target_group_attachment" "tga2" {
  target_group_arn = aws_lb_target_group.example.arn
  port             = 1433
  target_id        = "10.0.1.15"
}

// To create a NLB listener
resource "aws_lb_listener" "example" {
  load_balancer_arn = aws_lb.example.arn

  port     = 1433
  protocol = "TCP"
  default_action {
    target_group_arn = aws_lb_target_group.example.arn
    type             = "forward"
  }
}
