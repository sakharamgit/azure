
// To create a AWS VPC
resource "aws_vpc" "example" {
  cidr_block           = var.vpc_cidr_range
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-vpc"
    environment = var.env
  }
}

# Internet Gateway
resource "aws_internet_gateway" "example" {
  vpc_id = aws_vpc.example.id
  tags = {
    Name = "${var.prefix}-${var.project}-${var.env}-ig"
  }
}

# To create a route table
resource "aws_route_table" "example" {
  vpc_id = aws_vpc.example.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example.id
  }

  tags = {
    Name = "${var.prefix}-${var.project}-${var.env}-routetable"
  }
}

// To create a subnet in the VPC in AZ1
resource "aws_subnet" "az1" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.subnet1_cidr_block
  availability_zone = var.aws_availability_zone1

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-subnet1"
    environment = var.env
  }
}

// To create a subnet in the VPC in AZ2
resource "aws_subnet" "az2" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.subnet2_cidr_block
  availability_zone = var.aws_availability_zone2

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-subnet2"
    environment = var.env
  }
}

# To create a route table association
resource "aws_route_table_association" "example1" {
  subnet_id      = aws_subnet.az1.id
  route_table_id = aws_route_table.example.id
}
resource "aws_route_table_association" "example2" {
  subnet_id      = aws_subnet.az2.id
  route_table_id = aws_route_table.example.id
}
