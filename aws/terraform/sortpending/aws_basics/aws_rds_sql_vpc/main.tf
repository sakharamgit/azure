# To create a VPC in AWS
resource "aws_vpc" "example" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "${var.prefix}-${var.environment}-vpc"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "example" {
  vpc_id = aws_vpc.example.id
  tags = {
    Name = "${var.prefix}-${var.environment}-ig"
  }
}

# To create a route table
resource "aws_route_table" "example" {
  vpc_id = aws_vpc.example.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example.id
  }

  tags = {
    Name = "${var.prefix}-${var.environment}-routetable"
  }
}

# To create subnets in the AWS VPC
resource "aws_subnet" "example1" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.subnet_cidr1
  availability_zone = var.aws_availability_zone1

  tags = {
    Name = "${var.prefix}-${var.environment}-subnet"
  }
}

resource "aws_subnet" "example2" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.subnet_cidr2
  availability_zone = var.aws_availability_zone2

  tags = {
    Name = "${var.prefix}-${var.environment}-subnet"
  }
}

# To create a route table association
resource "aws_route_table_association" "example1" {
  subnet_id      = aws_subnet.example1.id
  route_table_id = aws_route_table.example.id
}
resource "aws_route_table_association" "example2" {
  subnet_id     = aws_subnet.example2.id
  route_table_id = aws_route_table.example.id
}


# To create a subnet Group
resource "aws_db_subnet_group" "example" {
  name        = "${var.prefix}-${var.environment}-rds-mssql-subnet-group"
  description = "The ${var.environment} rds-mssql private subnet group."
  subnet_ids  = [aws_subnet.example1.id, aws_subnet.example2.id]

  tags = {
    Name = "${var.environment}-rds-mssql-subnet-group"
    Env  = var.environment
  }
}

# To create a AWS secutiry group
resource "aws_security_group" "example" {
  name        = "${var.prefix}-${var.environment}-all-rds-mssql-internal"
  description = "${var.environment} allow all vpc traffic to rds mssql."
  vpc_id      = aws_vpc.example.id

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.environment}-all-rds-mssql-internal"
    Env  = var.environment
  }
}

# To create the MSSQL RDS instance
resource "aws_db_instance" "example" {
  depends_on                = [aws_db_subnet_group.example]
  identifier                = "${var.prefix}-${var.environment}-mssql"
  allocated_storage         = var.rds_allocated_storage
  license_model             = "license-included"
  storage_type              = "gp2"
  engine                    = "sqlserver-se"
  engine_version            = "12.00.4422.0.v1"
  instance_class            = "db.m4.large"
  multi_az                  = "false"
  username                  = var.sqlUsername
  password                  = var.sqlPassword
  publicly_accessible       = "true"
  vpc_security_group_ids    = [aws_security_group.example.id]
  db_subnet_group_name      = aws_db_subnet_group.example.id
  backup_retention_period   = 0
  skip_final_snapshot       = "true"
  final_snapshot_identifier = "${var.environment}-mssql-final-snapshot"
}