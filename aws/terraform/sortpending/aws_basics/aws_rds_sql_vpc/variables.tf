##################################################################################
# VARIABLES
##################################################################################

//AWS Variables
variable "aws_key_name" {}
variable "private_key_path" {}
variable "aws_username" {}
variable "sqlUsername" {}
variable "sqlPassword" {}

variable "aws_region" {
  default = "ap-southeast-1"
}

variable "aws_availability_zone1" {
  default = "ap-southeast-1a"
}

variable "aws_availability_zone2" {
  default = "ap-southeast-1b"
}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "subnet_cidr1" {
  type    = string
  default = "10.0.1.0/24"

}

variable "subnet_cidr2" {
  type    = string
  default = "10.0.2.0/24"

}

variable "rds_allocated_storage" {
  type    = string
  default = 20
}