// To create a VPC
resource "aws_vpc" "es-vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-vpc"
    environment = var.env
  }
}

// To create a subnet in the VPC
resource "aws_subnet" "es-subnet" {
  vpc_id            = aws_vpc.es-vpc.id
  cidr_block        = "172.16.0.0/24"
  availability_zone = var.aws_availability_zone

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-subnet"
    environment = var.env
  }
}

// To create a security group
resource "aws_security_group" "example" {
  name        = "${var.prefix}-${var.project}-${var.env}-sg"
  description = "AWS Security Group for EC2"
  vpc_id      = aws_vpc.example.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.example.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_vpc.example.cidr_block]
  }

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-sg"
    environment = var.env
  }
}

# To create elasticsearch domain
resource "aws_elasticsearch_domain" "esdomain" {
  domain_name           = "${var.prefix}-${var.project}-${var.env}-domain"
  elasticsearch_version = "7.10"

  cluster_config {
    instance_type = "m3.medium.elasticsearch"
  }

  tags = {
    Domain = "${var.prefix}-${var.project}-${var.env}-domain"
  }
}

# To create the access policy
resource "aws_elasticsearch_domain_policy" "main" {
  domain_name = aws_elasticsearch_domain.esdomain.domain_name

  access_policies = <<POLICIES
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Condition": {
                "IpAddress": {"aws:SourceIp": "*"}
            },
            "Resource": "${aws_elasticsearch_domain.esdomain.arn}/*"
        }
    ]
}
POLICIES
}