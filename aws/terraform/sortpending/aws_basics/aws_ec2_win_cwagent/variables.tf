##################################################################################
# VARIABLES
##################################################################################

//AWS Variables
variable "aws_key_name" {}
variable "private_key_path" {}
variable "aws_username" {}
variable "aws_region" {
  default = "ap-southeast-1"
}
variable "aws_availability_zone" {
  default = "ap-southeast-1a"
}

//Azure Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "cwagent"
}

variable "env" {
  type    = string
  default = "dev"
}

variable winusername {
  type    = string
  default = "winadmin"
}

variable winpassword {
  type    = string
  default = "P@ssword1!"
}

variable trusted_domain {
  type    = string
  default = "*.compute.amazonaws.com"
}

variable "start_cw_agent_path" {
  type = string
}

variable "cw_agent_config_path" {
  type = string
}