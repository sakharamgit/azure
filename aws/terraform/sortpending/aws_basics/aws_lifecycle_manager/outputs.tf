# Outputs

output "my_s3_bucket" {
  value = aws_s3_bucket.example.bucket
}

output "iam_role" {
  value = aws_iam_role.example.name
}