# Locals for NLB
locals {
  bv_info          = yamldecode(file("../../aws_projects/og_ec2/vpcintegration.yaml"))
  bv_name          = local.bv_info.bv_name
}

# To create the backup vault
resource "aws_backup_vault" "vpcint_backup_vault" {
  name        = local.bv_name
  #kms_key_arn = aws_kms_key.example.arn
} 
