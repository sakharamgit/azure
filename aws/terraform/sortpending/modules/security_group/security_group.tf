# Locals for NLB
locals {
  sg_info             = yamldecode(file("../../aws_projects/og_ec2/vpcintegration.yaml"))
  intg_sg_name        = local.sg_info.intg_sg_name
  intg_ingress_ports  = tomap(local.sg_info.intg_ingress_ports)
  vpc_id              = local.sg_info.vpc_id
  ec2sql_sg_name      = local.sg_info.ec2sql_sg_name
  sql_ingress_ports   = tomap(local.sg_info.sql_ingress_ports)
  environment         = local.sg_info.environment
}

#To get the vpc
data "aws_vpc" "vpc1" {
  id = local.vpc_id
}

# # To create a security group
# resource "aws_security_group" "intgsecgroup" {
#   name        = "vpcintg-security-group"
#   description = "AWS Security Group for EC2"
#   vpc_id      = data.aws_vpc.vpc1.id

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = [data.aws_vpc.vpc1.cidr_block]
#   }

#   tags = {
#     Name        = local.sg_name
#     environment = local.environment
#   }
# }


# resource "aws_security_group_rule" "ingress" {
#   for_each = local.sg_ingress_ports

#   type              = "ingress"
#   from_port         = lookup(each.value, "from_port")
#   to_port           = lookup(each.value, "to_port")
#   protocol          = lookup(each.value, "protocol")
#   cidr_blocks       = ["0.0.0.0/0"]
#   security_group_id = aws_security_group.intgsecgroup.id
# }


# To create a security group
resource "aws_security_group" "sqlsecgroup" {
  name        = local.ec2sql_sg_name
  description = "AWS Security Group for SQL EC2"
  vpc_id      = data.aws_vpc.vpc1.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [data.aws_vpc.vpc1.cidr_block]
  }

  tags = {
    Name        = local.ec2sql_sg_name
    environment = local.environment
  }
}


resource "aws_security_group_rule" "ingress" {
  for_each = local.sql_ingress_ports

  type              = "ingress"
  from_port         = lookup(each.value, "from_port")
  to_port           = lookup(each.value, "to_port")
  protocol          = lookup(each.value, "protocol")
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sqlsecgroup.id
}

# Output
output "sqlsecuritygroup" {
  value = aws_security_group.sqlsecgroup
}