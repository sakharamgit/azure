# Locals for ec2
locals {
  ec2_info          = yamldecode(file("../../aws_projects/og_ec2/vpcintegration.yaml"))
  ec2_name          = local.ec2_info.ec2_name
  vpc_name          = local.ec2_info.vpc_name
  ami_id            = local.ec2_info.ami_id
  subnet_ids        = local.ec2_info.sql_subnet_ids
  ec2sql_sg_name    = local.ec2_info.ec2sql_sg_name
  private_ip_count  = local.ec2_info.private_ip_count
  aws_key_name      = local.ec2_info.aws_key_name
  winusername       = local.ec2_info.winusername
  winpassword       = local.ec2_info.winpassword
  iam_profile_name  = local.ec2_info.iam_profile_name
}


// To retrieve the latest AMI for Windows Server 2019
data "aws_ami" "windows" {
  most_recent = true

  filter {
    name   = "image-id"
    values = [local.ami_id]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["801119661308"] # Canonical
}

# To run the below script on the ec2 instance creation
data "template_file" "winrm_config" {
  template = file("../../aws_projects/files/winrm_config.ps1")
  vars = {
    windows_username = local.winusername
    windows_password = local.winpassword
  }
}

//To get the IAM Profile
data "aws_iam_instance_profile" "ec2iamprofile" {
  name = local.iam_profile_name
}

// To create a network interface for EC2 in AZ1
resource "aws_network_interface" "nic" {
  count             = length(local.subnet_ids)
  subnet_id         = local.subnet_ids[count.index]
  security_groups   = [module.security_group.sqlsecuritygroup]
  private_ips_count = local.private_ip_count
}

output "network_ids" {
  value = aws_network_interface.nic
}

// To create Amazon EC2 Windows instance in AZ1
resource "aws_instance" "sqlec2" {
  count                       = length(aws_network_interface.nic)
  ami                         = data.aws_ami.windows.id
  instance_type               = "t2.xlarge"
  key_name                    = local.aws_key_name
  user_data                   = data.template_file.winrm_config.rendered
  iam_instance_profile        = module.aws_iam_instance_profile.sql_ec2_iam_inst_profile.name

  network_interface {
    network_interface_id = aws_network_interface.nic[count.index].id
    device_index         = 0
  }

  tags = {
    Name        = "sqlec2${count.index}"
  }
}

