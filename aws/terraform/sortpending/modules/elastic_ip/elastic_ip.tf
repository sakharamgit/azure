// To create elastic ip to associate with the network interfaces
resource "aws_eip" "eip1" {
  depends_on = [aws_network_interface.nic1]
  vpc                       = true
  network_interface         = aws_network_interface.nic1.id

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-eip1"
    environment = var.env
  }
}

resource "aws_eip" "eip2" {
  depends_on = [aws_network_interface.nic2]
  vpc                       = true
  network_interface         = aws_network_interface.nic2.id

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-eip2"
    environment = var.env
  }
}