# 1. Start the docker containers
docker-compose up

# 2. Check the running containers
docker ps

# 3. Open the bash in a container
docker exec -it <containerid> -- sh