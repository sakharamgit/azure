# URL Fetcher

## Introduction
This project contains a simple Python script to fetch and display the content of a given URL using the `urllib3` library.

## Getting Started

### Prerequisites
- Python 3.x
- `urllib3` library

### Installation
1. Clone the repository:
    ```sh
    git clone <repository-url>
    cd <repository-directory>
    ```
2. Install the required dependencies:
    ```sh
    pip install urllib3
    ```

## Usage
To fetch and display the content of a URL, run the script with the desired URL:
```sh
python setup.py