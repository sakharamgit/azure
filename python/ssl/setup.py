import urllib3

def fetch_url(url):
    # Disable SSL warnings (optional, for testing purposes)
    # urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    
    # Create a PoolManager instance
    http = urllib3.PoolManager()
    
    try:
        # Send a GET request
        response = http.request('GET', url)
        
        # Print status and response data
        print(f"Status Code: {response.status}")
        print("Response:")
        print(response.data.decode('utf-8'))  # Decode bytes to string
        
    except Exception as e:
        print(f"Error accessing URL: {e}")

# Example usage:
if __name__ == "__main__":
    url = 'https://api.github.com/users/Bard'
    fetch_url(url)
