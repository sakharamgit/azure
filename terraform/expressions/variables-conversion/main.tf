# List to Map output
output "a_listtomap" {
    value = ["${local.listconvertedtomap}"]
}

# Lists to Map output
output "b_liststomap_1to1" {
    value = ["${local.listsconvertedtomap1to1}"]
}

# Lists to Map output
output "c_liststomap_1tomany" {
    value = ["${local.listsconvertedtomap1tomany}"]
}