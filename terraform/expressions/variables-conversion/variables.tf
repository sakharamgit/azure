# List of Strings Declaration
variable names {
    type = list(string)
}

variable age {
    type = list(number)
}

variable subjects {
    type = list(string)
}