# Convert List to Map
locals {
    listconvertedtomap = {
        for name in var.names: name => { "type" = "string" } 
        }
}

# Convert Lists to Map 1to1
locals {
    listsconvertedtomap1to1 = {
        for index, name in var.names: name => var.age[index]
        }
}


# Convert Lists to Map 1toMany
locals {
    listsconvertedtomap1tomany = {
        for name in var.names: name => var.subjects
        }
}


