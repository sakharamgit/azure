
# output "a_listofobjects" {
#     value = [for vm in var.listofobjects : vm ]
# }

# output "a_listofobjects_filtered" {
#     value = [for vm in var.listofobjects[0] : vm] 
# }

# output "a_listofobjects_specific_attribute" {
#     value = [for vm in var.listofobjects : vm.size ]
# }

# output "a_listofobjects_filtered_attributes" {
#     value = [var.listofobjects[0].size ] 
# }

output "a_listofmaps" {
    value = [for subnet in var.listofmaps : subnet ] 
}

output "a_listofmapswithinlistofobjects" {
    value = [for vnet in var.listofobjectswithinlistofmaps : vnet ]
}

output "a_listofmapswithinlistofobjects_firstlayer" {
    value = [for vnet in var.listofobjectswithinlistofmaps : vnet.name ]
}

output "a_listofmapswithinlistofobjects_secondlayer" {
    value = [ var.listofmapswithinlistofojbects_secondlayer ]
}

# output "mapofobjects" {
#     value = [for user in var.mapofobjects : user ]
# }

# output "mapoflists" {
#     value = [for colors in var.mapoflists : colors]
# }

# output "mapoflists_filtered" {
#     value = [for colors in var.mapoflists["hall"] : colors]
# }