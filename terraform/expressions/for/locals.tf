# Defining a list of lists
locals {
  list_of_lists = [
    [
      { key = "luke", value = "jedi" },
      { key = "yoda", value = "jedi" }
    ],
    [
      { key = "darth", value = "sith" },
      { key = "palpatine", value = "sith" }
    ]
  ]

  merged_list = flatten(local.list_of_lists)
}