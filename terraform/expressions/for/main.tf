# For Loop - One Input and List Output with First Names
output "listinputlistoutput" {
  value = [for firstname in var.listoffirstnames : firstname]
}

# For Loop - One Input and List Output with First Names in Upper Case
output "listinputlistoutputupper" {
  value = [for firstname in var.listoffirstnames : upper(firstname)]
}

# For Loop - Two Inputs and List Output with Iterator
output "twolistinputonelistoutputiterator" {
  value = [for city, firstname in var.listoffirstnames : city]
}

# For Loop - Two Inputs and List Output with Map
output "twolistinputonelistoutputmap" {
  value = {for person in var.routine : person.name => person.city }
}

# For Loop - Two Inputs and List Output with Map
output "twolistinputonelistoutputmapkeys" {
  value = keys({for person in var.routine : person.name => person.city })
}

# For Loop - Two Inputs and List Output with Map
output "twolistinputonelistoutputmapvalues" {
  value = values({for person in var.routine : person.name => person.city })
}

# Merge List - Merge
output "mergelist" {
  value = merge({for sno, person in concat(var.listoffirstnames, var.listoflastnames) : sno => person })
}

# Merge List - flatten
output "mergeflatten" {
    value = flatten(local.list_of_lists)
}

