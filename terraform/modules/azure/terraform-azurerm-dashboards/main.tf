


resource "azurerm_dashboard" "my-board" {
  name                = var.dashboard_name
  resource_group_name = var.resource_group_name
  location            = var.location

  tags       = merge(var.resource_tags, var.deployment_tags)

  dashboard_properties = var.dashboard_properties
}