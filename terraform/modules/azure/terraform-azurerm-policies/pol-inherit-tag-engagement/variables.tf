variable "name" {
  description = "(Required) Name of the policy"
  type        = string
}
variable "policy_type" {
  description = "(Required) Type of the policy"
  type        = string
}
variable "mode" {
  description = "(Required) Mode of the policy"
  type        = string
}
variable "display_name" {
  description = "(Required) Display name of the policy"
  type        = string
}
variable "description" {
  description = "(Required) Description of the policy"
  type        = string
}
variable "scope" {
  description = "(Required) Scope of the policy"
  type        = string
}
variable "not_scopes" {
  description = "(Optional) scopes to exclude from policy"
}