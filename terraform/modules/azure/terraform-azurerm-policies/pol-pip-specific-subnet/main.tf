resource "azurerm_policy_definition" "pol-pip-specific-subnet" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description

  parameters = <<PARAMETERS
	{
      "subnetIds": {
        "type": "Array",
        "metadata": {
          "displayName": "List of Subnets that can use a public IP",
          "description": "The subnetIds parameter must be provided with a list of subnets in format of resource ID (e.g /saz_solutionscriptions/{saz_solutionscription_id}/resourcegroups/{resource_group}/providers/microsoft.network/virtualnetworks/{vnet}/subnets/{subnet-name})"
        }
      }
    }
  	PARAMETERS

  policy_rule = <<POLICY_RULE
	{
      "if": {
        "allOf": [
          {
            "field": "type",
            "equals": "Microsoft.Network/networkInterfaces"
          },
          {
            "not": {
              "field": "Microsoft.Network/networkInterfaces/ipconfigurations[*].publicIpAddress.id",
              "exists": false
            }
          },
          {
            "not": {
              "field": "Microsoft.Network/networkInterfaces/ipconfigurations[*].subnet.id",
              "in": "[parameters('subnetIds')]"
            }
          }
        ]
      },
      "then": {
        "effect": "deny"
      }
    }
	POLICY_RULE
}

resource "azurerm_policy_assignment" "pol-pip-specific-subnet" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-pip-specific-subnet.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-pip-specific-subnet]

  parameters = jsonencode({
    subnetIds = {
      value = var.listOfSubnets
    }
  })
}
