variable "name" {
  description = "(Required) Name of the policy"
  type        = string
}
variable "policy_type" {
  description = "(Required) Type of the policy"
  type        = string
}
variable "mode" {
  description = "(Required) Mode of the policy"
  type        = string
}
variable "display_name" {
  description = "(Required) Display name for the policy"
  type        = string
}
variable "description" {
  description = "(Required) Description for the policy"
  type        = string
}

variable "strongType" {
  description = "(Required) Type for the policy"
  type        = string
}

variable "scope" {
  description = "(Required) scope the policy"
  type        = string
}
variable "not_scopes" {
  description = "(Optional) scopes to exclude from policy"
}
variable "listOfAllowedLocations" {
  description = "(Required) list of allowed locations to include in policy"
  type        = list(string)
}
