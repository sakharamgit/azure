resource "azurerm_policy_definition" "pol-deny-sourceaddressprefix-1024ip" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description
  metadata     = <<METADATA
    {
    "category": "Network Security Groups"
    }
  METADATA

  policy_rule = <<POLICY_RULE
 {
    "if": {
        "allOf": [
          {
            "field": "Microsoft.Network/networkSecurityGroups/securityRules/direction",
            "equals": "inbound"
          },
          {
            "field": "Microsoft.Network/networkSecurityGroups/securityRules/access",
            "equals": "Allow"
          },
          {
            "field": "Microsoft.Network/networkSecurityGroups/securityRules/sourceAddressPrefix",
            "contains": "/"
          },
          {
            "value": "[if(not(empty(field('Microsoft.Network/networkSecurityGroups/securityRules/sourceAddressPrefix'))),last(split(field('Microsoft.Network/networkSecurityGroups/securityRules/sourceAddressPrefix'),'/')),'300')]",
            "less": "22"
          }
        ]
      },
      "then": {
        "effect": "[parameters('effect')]"
      }
      }
 POLICY_RULE

  parameters = <<PARAMETERS
    {
    "effect": {
          "type": "string",
          "allowedValues": [
            "deny",
            "audit"
          ],
          "metadata": {
            "description": "Effect required to apply in this policy.",
            "displayName": "Effect required to apply in this policy"
            }
        }
    }
PARAMETERS
}
resource "azurerm_policy_assignment" "pol-deny-sourceaddressprefix-1024ip" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-deny-sourceaddressprefix-1024ip.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-deny-sourceaddressprefix-1024ip]
  parameters = jsonencode({
    effect = {
      value = var.effect
    }
  })
}